<?php if ( !empty( $attributes['redirect'] ))  : ?>
    <meta http-equiv="refresh" content="0; URL='<?php echo $attributes['redirect'];?>'" />
<?php else: ?>


<div class="admin-access-container">

    <?php if ( !empty($attributes['error'])) : ?>
      <span>  <?php echo $attributes['error']; ?> </span>
    <?php else : ?>

        <!--
            <div class="search-order-user">
                <div class="site-search" style="display: block;">
                    <?php the_widget( 'WC_Widget_Product_Search', 'title=' ); ?>
                </div>
            </div>
        -->
    <div class="admin-setting-grid">
        <div class="jde-row" id="top-buttons">
             <div class="jde-col-sm-6" id="button-pending-order">
                 <a href="<?php echo home_url() . '/admin-pending-order'?>">
                     <div class="outline-div">
                         <div class="func-icon"><i class="fa fa-align-justify"></i></div>
                         <div class="func-ttl"><?php _e('View Pending Order Execute','jde-admin'); ?>
                         </div>
                     </div>
                 </a>
             </div>
            <div class="jde-col-sm-6" id="button-order-history">
                <a href="<?php echo home_url() . '/admin-order-history'?>">
                    <div class="outline-div">
                        <div class="func-icon">
                            <i class="fa fa-history"></i>
                        </div>
                        <div class="func-ttl"><?php _e('View Order History','jde-admin'); ?>
                        </div>
                    </div>
                </a>
           </div>
        </div>

        <div class="jde-row" id="bottom-buttons">
            <div class="jde-col-sm-6" id="button-edit-user">
                <a href="<?php echo home_url() . '/admin-edit-user'?>" >
                    <div class="outline-div">
                        <div class="func-icon">
                            <i class="fa fa-user-plus"></i>
                        </div>
                        <div class="func-ttl"><?php _e('Edit User','jde-admin'); ?>
                        </div>
                    </div>
                </a>
            </div>
            <div class="jde-col-sm-6" id="button-admin-setting" >
                <a href="<?php echo home_url() . '/admin-setting'?>">
                    <div class="outline-div">
                        <div class="func-icon">
                            <i class="fa fa-cogs"></i>
                        </div>
                        <div class="func-ttl"><?php _e('Admin Setting','jde-admin'); ?>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <?php endif; ?>

</div>


<?php endif; ?>