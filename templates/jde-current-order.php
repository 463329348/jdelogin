
<?php if ( !empty( $attributes['redirect'] ))  : ?>
<meta http-equiv="refresh" content="0; URL='<?php echo $attributes['redirect'];?>'" />

    <?php else : ?>


<div class="purchaser-home-container">

    <div class="jde-row title" style="background-color: #d3d3d3;">
        <div class="jde-col-sm-4  new-order order-op"><a href="<?php echo home_url() . '/purchaser-home-cross'; ?>"><?php  _e('New Order','jde-order'); ?></a></div>
        <div class="jde-col-sm-4  current-order order-op selected"><?php  _e('Current Order','jde-order'); ?></div>
        <div class="jde-col-sm-4  order-history order-op"><a href="<?php echo home_url() . '/jde-order-history'; ?>"><?php  _e('Order History','jde-order'); ?></a></div>
    </div>

    <div class="current-order-details">
        <div class="title"><span><b><?php  _e('My Current Orders','jde-order'); ?></b></span></div>

        <?php foreach ( $attributes['orders'] as $order)  : ?>

        <div style="border-top: 1px solid darkgrey;">
            <span style="font-weight: bold;"><?php  _e('Order No.','jde-order'); ?>: </span> <span><?php echo $order['no']; ?></span><br>
            <span><?php  _e('Order Date','jde-order'); ?>: </span> <span><?php echo $order['date']; ?></span><br>
            <span><?php  _e('Units','jde-order'); ?>: </span> <span><?php echo $order['total_units']; ?></span> <br>
            <span><?php  _e('Total','jde-order'); ?>: </span> <span><?php echo get_woocommerce_currency_symbol() . number_format( $order['total_price'],2); ?></span><br>
            <span><?php  _e('By','jde-order'); ?>: </span> <span><?php echo $order['op_name']; ?></span>
        </div>
        <div class="order-status"> <span><?php  _e('Order Status','jde-order'); ?>: </span> <span class="order-item-status"><?php echo $order['status']; ?></span><br></div>

        <div class="order-item-list">
            <div class="order-item-list-ttl">
                <span><?php  _e('Order Items List','jde-order'); ?>:</span>
            </div>
            <div class="order-item-content">
                <table>
                    <thead>
                    <th style="width: 150px;"><?php  _e('Product Name','jde-order'); ?></th>
                    <th><?php  _e('QTY','jde-order'); ?></th>
                    <th><?php  _e('Status','jde-order'); ?></th>
                    <th><?php  _e('Op','jde-order'); ?></th>
                    </thead>
                    <tbody>
                    <?php foreach ( $order['lines'] as $line )  : ?>
                        <tr>
                            <td><?php echo  $line['name']; ?></td>
                            <td><?php echo  $line['qty']; ?></td>
                            <td><?php echo  $line['status']; ?></td>
                            <td  data-oid="<?php echo $order['no'] ;?>" data-pid="<?php echo $line['pid'] ;?>" data-lineid="<?php echo $line['lineid']; ?>">
                                <button class="btn-order-details"><?php  _e('Details','jde-order'); ?></button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <?php endforeach; ?>

    </div>

</div>

<div class="jde-popup-bgd"></div>
<div class="jde-popup order-details">
    <a href="#" class="remove"></a>
    <div class="order-details-content">
        <ul style="list-style: none;">
            <li id="order-p-code"><div class="line-title"><?php _e('CODE','jde-order');?> </div> <div class="line-value"></div></li>
            <li id="order-p-name"><div class="line-title"><?php _e('PRODUCT NAME','jde-order');?> </div> <div class="line-value"></div></li>
            <li id="order-p-category"><div class="line-title"><?php _e('CATEGORY','jde-order');?> </div> <div class="line-value"></div></li>
            <li id="order-p-vintage"><div class="line-title"><?php _e('VINTAGE','jde-order');?> </div> <div class="line-value"></div></li>
            <li id="order-p-unit"><div class="line-title"><?php _e('UNIT','jde-order');?> </div> <div class="line-value"></div></li>
            <li id="order-p-qty"><div class="line-title"><?php _e('QTY','jde-order');?> </div> <div class="line-value"></div></li>
        </ul>
    </div>
</div>


<?php endif; ?>