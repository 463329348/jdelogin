
<?php if ( !empty( $attributes['redirect'] ))  : ?>
    <meta http-equiv="refresh" content="0; URL='<?php echo $attributes['redirect'];?>'" />


<?php else :?>
<div class="jde-checkout-header">
    <div class="jde-col-sm-4 shipping done"><span><?php  _e('SHIPPING','jde-checkout');?></span></div>
    <div class="jde-col-sm-4 review"><span><?php  _e('ORDER REVIEW','jde-checkout');?></span></div>
    <div class="jde-col-sm-4 confirm"><span><?php  _e('CONFIRM','jde-checkout');?></span></div>
</div>

<div class="jde-checkout-container">
    <div class="ttl"><?php _e('CUSTOMER DETAILS','jde-checkout');?></div>
    <div class="details">
        <?php

            $company_name = '';
            $op_name = '';
            if ( isset($attributes['customer_cname'])) $company_name = $attributes['customer_cname'];
            if ( isset($attributes['op_name'] ) )  $op_name = $attributes['op_name'];
            $telephone = '';


        ?>
        <div class="row-lable">
            <div class="header"><span><?php  _e('COMPANY NAME','jde-checkout');?></span></div>
            <div class="content"><input name="shipping_first_name" value="<?php echo $company_name; ?>" disabled></div>
        </div>

        <div class="row-lable">
            <div class="header"><span><?php  _e('ORDER BY','jde-checkout');?></span></div>
            <div class="content"><input name="op_name" value="<?php echo $op_name; ?>" disabled></div>
        </div>

        <div class="row-lable">
            <div class="header"><span><?php  _e('ADDRESS','jde-checkout');?></span></div>
            <div class="content">
                <select name="shipping_address" id="jde-shipping-address">
                    <?php foreach ( $attributes['addresses'] as $id => $address ) : ?>
                        <?php if ( empty($telephone) ) { $telephone = $address['telephone'];}   ?>
                        <option data-telephone="<?php echo $address['telephone']; ?>" value="<?php echo $address['id']; ?>"><?php echo $address['address']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <div class="row-lable">
            <div class="header"><span><?php  _e('TEL','jde-checkout');?></span></div>
            <div class="content"><input id="jde-shipping-phone" name="shipping_phone" value="<?php echo $telephone;?>" disabled></div>
        </div>

    </div>


    <div class="jde-checkout-extra">
        <span><b><?php _e('NOTE','jde-checkout');?></b></span>
        <br>
        <textarea class="notes"  id="jde-checkout-notes" name="order_comments" cols="20" rows="8" ><?php echo $attributes['note'];?></textarea>
    </div>

</div>

<div class="jde-checkout-review" style="display: none;">
    <div class="ttl"><?php  _e('ORDER SUMMERY','jde-checkout');?></div>
    <div class="products-list">
    <table>
        <thead>
        <th><?php  _e('CODE','jde-checkout');?></th>

        <th><?php  _e('QTY','jde-checkout');?></th>
        <?php if ( $attributes['show_price']) : ?>
        <th class="price-ttl"><?php  _e('UNIT PRICE','jde-checkout');?></th>
        <th class="subtotal-ttl"><?php  _e('SUB TOTAL','jde-checkout');?></th>
        <?php endif; ?>
        <th><?php  _e('DESCRIPTION','jde-checkout');?></th>
        </thead>
        <tbody>
        <?php foreach (  $attributes['products'] as $product ) : ?>
            <tr>
                <td><?php echo $product['sku']; ?></td>

                <td><?php echo $product['qty']; ?></td>
            <?php if ( $attributes['show_price']) : ?>
                <td class="price-value"><?php echo get_woocommerce_currency_symbol() . number_format($product['price'],2); ?></td>
                <td class="subtotal-value"><?php echo get_woocommerce_currency_symbol() . number_format($product['subtotal'],2); ?></td>
            <?php endif; ?>
                <td><?php echo $product['name']; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
        </div>
</div>

<div class="jde-popup-bgd">
</div>
<div class="jde-popup jde-checkout-result">
    <a href="#" class="remove"></a>
    <span style="font-size: 80px;font-size: 80px;color: firebrick;"><i class="fa fa-check"></i></span><br>
    <span><?php _e('THANK YOU','jde-order-success'); ?></span> <br>
    <span><?php _e('YOUR REQUEST HAVE BEEN SENT TO','jde-order-success'); ?> <br><span style="color: #b22222;"><?php _e('SUMMERGATE','jde-order-success'); ?></span></span> <br>
    <br>

    <button data-redirect="<?php echo esc_url(home_url() . '/purchaser-home'); ?>" id="order-checkout-result-ok-btn"><?php _e('OK','jde-checkout');?></button>
    <!--
    <a href="<?php echo esc_url( home_url() . '/purchaser-home') ; ?>"><button>View Products</button> </a>
    <a href="<?php echo esc_url( home_url() . '/jde-orders') ; ?>"><button>View By Order</button> </a>
    -->
</div>


<div class="jde-shopping-cart-footer">
    <a class="jde-col-sm-6 new-product"href="<?php echo esc_url( home_url() . '/purchaser-home') ; ?>">
    <div>
        <span> <?php _e('ADD NEW <br> PRODUCT','jde-checkout');?></span>
    </div> </a>

<a class="jde-col-sm-6 checkout-next" href="javascript:void(0);">
    <div >
        <span><?php _e('NEXT','jde-checkout');?></span>
    </div></a>

<a class="jde-col-sm-6 checkout-confirm" href="javascript:void(0);" style="display: none;">
    <div >
        <span><?php _e('CONFIRM','jde-checkout');?></span>
    </div></a>
</div>


<?php endif; ?>
