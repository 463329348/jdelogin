
<?php if ( !empty( $attributes['redirect'] ))  : ?>
    <meta http-equiv="refresh" content="0; URL='<?php echo $attributes['redirect'];?>'" />

<?php else : ?>


<div class="purchaser-home-container" >

    <div class="jde-row title" style="background-color: #d3d3d3;">
        <div class="jde-col-sm-4  new-order order-op selected"><?php  _e('New Order','jde-login'); ?></div>
        <div class="jde-col-sm-4  current-order order-op"><a href="<?php echo home_url() . '/jde-current-order'; ?>"><?php  _e('Current Order','jde-login'); ?></a></div>
        <div class="jde-col-sm-4  order-history order-op"><a href="<?php echo home_url() . '/jde-order-history'; ?>"><?php  _e('Order History','jde-login'); ?></a></div>
    </div>

    <div class="jde-row sort" style="background-color: white;">
        <div class="jde-col-sm-4 order-sort">
            <span id="order-sort-grid"  class="selected"><i class="fa fa-th-large"></i></span>
            <span id="order-sort-list"><i class="fa fa-list-ul"></i></span>
            <span id="order-search"><i class="fa fa-search"></i></span>
        </div>
        <div class="jde-col-sm-4 order-search" ><input data-page="p" style="width: 100%;" type="text" name="search_key"></div>
        <div class="jde-col-sm-4 order-sortby"><span style="font-size: 22px;"><span style="margin-right: 5px;"><?php  _e('sort by','jde-login'); ?></span><span style="color:#d61f4d;"><i class="fa fa-angle-right"></i></span></span></div>
    </div>
    <div class="jde-sortby-submenus">
        <div class="jde-arrow-up"></div>
        <ul>
            <li class="selected"><a class="jde-sort-option" data-page="p" data-key="none" href="#"> <?php _e('None','jde-product'); ?> </a></li>
          <!--  <li><a class="jde-sort-option"  data-page="p"  data-key="price-asc" href="#"> <?php _e('Price ASC','jde-product'); ?> </a></li>
            <li><a class="jde-sort-option"   data-page="p"  data-key="price-desc" href="#"> <?php _e('Price DESC','jde-product'); ?> </a></li> -->
            <li><a class="jde-sort-option"  data-page="p"  data-key="name-asc" href="#"> <?php _e('Name(A-Z)','jde-product'); ?> </a></li>
            <li><a class="jde-sort-option"  data-page="p"  data-key="name-desc" href="#"> <?php _e('Name(Z-A)','jde-product'); ?> </a></li>
        </ul>

    </div>


    <div class="jde-row jde-list-products" id="jde-purchaser-list-products" style="padding-top:90px;">

        <?php foreach ( $attributes['products'] as $product ) : ?>
        <?php echo JdeUtils::get_template_html('jde_grid_product_box',array('product' => $product)); ?>
        <?php endforeach; ?>
    </div>

    <div class="jde-row jde-oneList-products" style="padding-top:40px;display: none;">
        <?php foreach ( $attributes['products'] as $product ) : ?>
            <?php echo JdeUtils::get_template_html('jde_list_product_box',array('product' => $product,'show_price' => $attributes['show_price'])); ?>
        <?php endforeach; ?>
    </div>

    <input type="hidden" id="purchaser-home-page-index" name="purchaser_home_page_index" value="0">
</div>



<?php endif; ?>
<div class="loading-more-flag" style="display:none;"><img src="<?php echo get_stylesheet_directory_uri() . '/images/loading_more.gif';?>"> </div>