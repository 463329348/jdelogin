<?php if ( !empty( $attributes['redirect'] ))  : ?>
    <meta http-equiv="refresh" content="0; URL='<?php echo $attributes['redirect'];?>'" />


<?php else : ?>

<div class="admin-edit-user-container" style="text-align: center;">
<!--
    <div class="search-order-user">
        <div class="site-search" style="display: block;">
            <?php the_widget( 'WC_Widget_Product_Search', 'title=' ); ?>
        </div>
    </div>
-->

    <span> <?php _e('Tel.','jde-admin'); ?> : 800-820-6929 </span> <br/>

    <span> <?php _e('Email','jde-admin'); ?> : infor@summergate.com </span> <br/>
</div>

<?php endif; ?>