<div class = "jde-list-box">
    <div class="jde-listProduct-box" data-sku="<?php echo $attributes['product']->get_sku(); ?>">
        <i class="right"></i>
        <div class="listProduct-details">
            <ul><li><?php echo $attributes['product']->get_name(); ?></li></ul>
        </div>
        <div class="listPrice-details">
            <ul><li><?php
                    if ( $attributes['show_price'] ) {

                        require_once ( get_stylesheet_directory()  . '/includes/jde-internal-user.php');
                        require_once ( get_stylesheet_directory()  . '/includes/jde-customer-ref.php');
                        $jdeInternalUserObj = new JdeInternalUser();
                        $jdeCustomerRefObj = new JdeCustomerRef();
                        $my_customer_id = $jdeInternalUserObj->getMyCustomerId();
                        $contract_price = $jdeCustomerRefObj->getCrossPrice($my_customer_id,$attributes['product']->get_sku());
                        if ( empty($contract_price) ) {
                            $contract_price = $attributes['product']->get_regular_price();
                        }
                        echo get_woocommerce_currency_symbol() .number_format($contract_price,2);
                    }
                    ?>
                </li></ul>
        </div>
        <?php
        $min_unit = $attributes['product']->get_attribute('min_unit');
        if ( empty($min_unit) ) $min_unit = 1;
        ?>
        <div class="product-cartButtons" data-unit="<?php echo $min_unit;?>">
            <span class="hideCart"><i class="fa fa-shopping-cart"></i></span>
        </div>

        <div class="product-hideButtons" data-unit="<?php echo $min_unit;?>">
            <div class="text-details">
                <?php echo __('(Minimum Order Quality ','jde-product') . $min_unit . ')'; ?>
            </div>
            <div class="buttons-details" data-unit="<?php echo $min_unit;?>" >
                <ul>
                    <?php  _e('QTY','jde-product'); ?>
                </ul>
                <span class="minus"><i class="fa fa-minus"></i></span>
                <input class="qty" type="number" name="qty" value="<?php echo $min_unit;?>">
                <span class="add"><i class="fa fa-plus"></i></span>
            </div>
        </div>
        <div class="product-hideDetails">
            <table class="product-tableDetails">
                <tr>
                    <td><?php  _e('GRAPE','jde-product'); ?><span><?php echo ":";echo " ";?><?php echo $attributes['product']->get_attribute('grape') ;?></span></td>
                    <td><?php  _e('BOTTLE SIZE','jde-product'); ?><span><?php echo ":";echo " ";?><?php echo $attributes['product']->get_attribute('bottle_size');?></span></td>
                </tr>
                <tr>
                    <td><?php  _e('CODE','jde-product'); ?><span><?php echo ":";echo " ";?><?php echo $attributes['product']->get_sku(); ?></span></td>
                    <td><?php  _e('COUNTRY','jde-product'); ?><span><?php echo ":";echo " ";?><?php echo $attributes['product']->get_attribute('country'); ?></span></td>
                </tr>
                <tr>
                    <td><?php  _e('COLOR','jde-product'); ?><span><?php echo ":";echo " ";?><?php echo $attributes['product']->get_attribute('color'); ?></span></td>
                    <td><?php  _e('REGION','jde-product'); ?><span><?php echo ":";echo " ";?><?php echo $attributes['product']->get_attribute('region'); ?></span></td>
                </tr>
            </table>
        </div>
    </div>
</div>