<?php if ( !empty( $attributes['redirect'] ))  : ?>
    <meta http-equiv="refresh" content="0; URL='<?php echo $attributes['redirect'];?>'" />

    <?php else: ?>


<div class="admin-order-history-container">

    <div class="pending-order-ttl"> <span><?php _e('ORDER HISTORY','jde-order');?> </span></div>

    <div class="history-order-list">
        <table style="table-layout: fixed;" class="pending-order-table">
            <thead style="font-size: 14px;">
            <th><?php _e('ORDER NO.','jde-order');?></th>
            <th><?php _e('DATE','jde-order');?></th>
            <th><?php _e('CUSTOMER','jde-order');?></th>
            <th><?php _e('TOTAL','jde-order');?></th>
            </thead>
            <tbody>

            <?php foreach  ( $attributes['orders']  as $order ) : ?>
                <?php $index=1; ?>
                <tr class="order-line" id="order-line-<?php echo $order['id']; ?>">
                    <td><i class="fa fa-sort-down"></i> &nbsp; <?php echo $order['id'] ;?></td>
                    <td style="font-size: 14px;"><?php echo $order['date'] ;?><br></td>
                    <td><?php echo $order['customer'] ;?></td>
                    <td><?php echo get_woocommerce_currency_symbol() . number_format($order['total'],2) ;?></td>
                </tr>
                <tr class="order-details-line">
                    <td class="details" colspan="4">
                        <div class="order-detail-topbar">
                            <span><?php _e('ORDER BY','jde-order');?> : <?php echo $order['opname'] ;?></span><br>
                        <span><?php _e('Remark','jde-order');?> : <?php echo $order['note'];?></span>
                        </div>
                        <div class="order-details-content">
                            <table>
                                <thead>
                                <th><?php _e('ITEM','jde-order');?></th>
                                <th><?php _e('CODE','jde-order');?></th>
                                <th><?php _e('QTY','jde-order');?></th>
                                <th><?php _e('PRICE','jde-order');?></th>
                                <th><?php _e('TOTAL','jde-order');?></th>
                                <th></th>
                                </thead>
                                <tbody>
                                <?php foreach ( $order['lines'] as $line ): ?>
                                    <tr id="order-item-<?php echo $order['id'] . '-' . $line['id']; ?>" data-oid="<?php echo $order['id'];?>" data-lineid="<?php echo $line['id'];?>">
                                        <td><?php echo $index++;?></td>
                                        <td><?php echo $line['code'];?></td>
                                        <td>
                                                <?php echo $line['qty'];?>
                                        </td>
                                        <td>
                                            <?php echo get_woocommerce_currency_symbol() . number_format($line['price'],2);?>
                                        </td>
                                        <td><?php echo get_woocommerce_currency_symbol() . number_format($line['subtotal'],2);?></td>
                                    </tr>

                                    <tr class="promotion"><td class="promotion-box" colspan="6"><?php _e('Status:','jde-order');?> <?php echo $line['status'];?> | <?php _e('Promotion:','jde-order');?> <?php echo $line['promotion'];?></td></tr>

                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="loading-more-flag" style="display:none;"><img src="<?php echo get_stylesheet_directory_uri() . '/images/loading_more.gif';?>"> </div>
    </div>

    <input type="hidden" id="pending-order-page-index" name="pending-order-page-index" value="1">


</div>


<?php endif; ?>