<?php if ( !empty($attributes['redirect'] )) : ?>
    <meta http-equiv="refresh" content="0; URL='<?php echo $attributes['redirect'];?>'" />

    <?php else : ?>


<div class="login-form-container">

    <?php

    $args = array(
        'echo' => true,
        // Default 'redirect' value takes the user back to the request URI.
        'redirect' => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
        'form_id' => 'loginform',
        'label_username' => __( 'Username' ),
        'label_password' => __( 'Password' ),
        'label_remember' => __( 'Remember Me' ),
        'label_log_in' => __( 'Log In' ),
        'id_username' => 'user_login',
        'id_password' => 'user_pass',
        'id_remember' => 'rememberme',
        'id_submit' => 'wp-submit',
        'remember' => false,
        'value_username' => '',
        // Set 'value_remember' to true to default the "Remember me" checkbox to checked.
        'value_remember' => false,
    );


    $clientHeaderImageUrl = get_stylesheet_directory_uri() . '/images/defheader.jpg';
    $wechatLogoImageUrl = get_stylesheet_directory_uri() . '/images/wechat_logo.png';
    $clientName = 'JDE';

    // get errors if existing
    $errors = '';
     if ( count( $attributes['errors'] ) > 0 ) {
        foreach ( $attributes['errors'] as $error ) {
            $errors .= '<p class="login-error"> ' . $error . '</p>';
        }
    }

    // try to get nickname and head image url
    if ( isset($attributes['wxid']) ) {
        $wxid = $attributes['wxid'];
        $nicknameOptionKey = 'wx-jde-'.$wxid.'-nickname';
        $headOptionKey = 'wx-jde-'.$wxid.'-header';
        $clientNameTmp = get_option($nicknameOptionKey);
        if ( !empty($clientNameTmp) ) $clientName = $clientNameTmp;
        $clientHeaderImageUrlTmp = get_option($headOptionKey);
        if ( !empty($clientHeaderImageUrlTmp) ) $clientHeaderImageUrl = $clientHeaderImageUrlTmp;
    }

    $form = '

        <div class="jde-login-header">
        <div style="margin-bottom: 20px;">
        <span id="header-ttl">Summergate</span><br>
        <span>FINE WINES & SPIRITS </span></div>
        <img id="img-client-header" src="' . $clientHeaderImageUrl . '" />
        <span id="header-name">'.$clientName.'</span>

        </div>
		<form name="' . $args['form_id'] . '" id="' . $args['form_id'] . '" action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post">
			'  . '
			<p class="login-username">
				<label for="' . esc_attr( $args['id_username'] ) . '">' . esc_html( $args['label_username'] ) . '</label> <br>
				<input type="text" placeholder="Type your name here" name="log" id="' . esc_attr( $args['id_username'] ) . '" class="input" value="' . esc_attr( $args['value_username'] ) . '" size="20" />
			</p>
			<p class="login-password">
				<label for="' . esc_attr( $args['id_password'] ) . '">' . esc_html( $args['label_password'] ) . '</label><br>
				<input type="password" name="pwd" id="' . esc_attr( $args['id_password'] ) . '" class="input" value="" size="20" />
			</p>';

    if ( !empty($errors) ) {
        $form .= $errors;
    }

    if ( isset($attributes['wxid']) ) {
        $form .= '<input type="hidden" name="wxid" value="' . $attributes['wxid']  . '">';
    }
    $form .= '<p class="login-submit jde-login-submit">

    <input type="checkbox" id="jde-policy-check" name="privacy_check" checked="true"><label style="color:white;">' . __('You agree that the program use your Wechat portrait and your WeChat name','jde-login') .
        '</label> <br>
              <input type="submit" name="wp-submit" id="' . esc_attr( $args['id_submit'] ) . '" class="button button-primary" value="' . esc_attr( $args['label_log_in'] ) . '" />
				<input type="hidden" name="redirect_to" value="' . esc_url( $args['redirect'] ) . '" />
			</p>
			' . '
		</form> ';


	 $form .= '<div class="jde-login-bottom">
		    <span>Only require login when first time use</span>
		    <img src="' . $wechatLogoImageUrl . '" />
		</div>
		';

    echo $form;

    ?>

</div>


<?php endif; ?>
