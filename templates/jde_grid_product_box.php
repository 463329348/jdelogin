<div class="jde-product-box" data-sku="<?php echo $attributes['product']->get_sku(); ?>">
    <div class="product-content" style="display: block">
        <div class="product-image">
            <?php echo $attributes['product']->get_image(); ?>
        </div>
        <div class="product-details">
            <ul>
                <li><div class="jde-ptext-cut"><?php echo $attributes['product']->get_name(); ?></div></li>
                <li><div class="jde-ptext-cut"><?php echo $attributes['product']->get_attribute('grape'); ?></div></li>
                <li><?php echo $attributes['product']->get_attribute('bottle_size'); ?></li>
                <li><?php echo $attributes['product']->get_sku(); ?></li>
            </ul>
            <ul>
                <li><?php echo $attributes['product']->get_attribute('country'); ?></li>
                <li><?php
                    if ( $attributes['product']->get_attribute('vintage') == 0 ) {
                        echo '';
                    } else {
                        echo $attributes['product']->get_attribute('vintage');
                    }
                    ?></li>
                <li><?php echo $attributes['product']->get_attribute('color'); ?></li>
                <li><?php echo $attributes['product']->get_attribute('region'); ?></li>
            </ul>
        </div>
    </div>

    <?php
    $min_unit = $attributes['product']->get_attribute('min_unit');
    if ( empty($min_unit) ) $min_unit = 1;
    ?>
    <div class="product-buttons" data-unit="<?php echo $min_unit;?>">
        <span class="add"><i class="fa fa-plus"></i></span>
        <input class="qty" type="number" name="qty" value="<?php echo $min_unit;?>">
        <span class="minus"><i class="fa fa-minus"></i></span>
        <span class="cart"><i class="fa fa-shopping-cart"></i></span>
    </div>
</div>