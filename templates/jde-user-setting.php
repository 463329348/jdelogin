<?php if ( !empty( $attributes['redirect'] ))  : ?>
    <meta http-equiv="refresh" content="0; URL='<?php echo $attributes['redirect'];?>'" />
<?php else : ?>
    <div class="admin-edit-user-container" style="height: 100vh; background-color: white;">
<!--
        <div class="search-order-user">
            <div class="site-search" style="display: block;">
                <?php the_widget( 'WC_Widget_Product_Search', 'title=' ); ?>
            </div>
        </div>
-->

        <div class="user-center-setting">
            <span style="font-weight: bold;"><?php _e('My Details','jde-user-center');?></span><br><br>
            <span><?php _e('User Name:','jde-user-center'); ?> <?php echo $attributes['user_name']; ?> </span> <br>
            <span><?php _e('Customer Name:','jde-user-center'); ?> <?php echo $attributes['customer_name']; ?> </span>
        </div>

        <div style="text-align: center; margin-top: 20px;">
            <button class="jde-button" id="btn-unbind-me" data-uid="<?php echo $attributes['uid'];?>"><?php _e('Unbind','jde-admin');?></button>
        </div>


        <div style="text-align: center; margin-top: 20px;">
            <button class="jde-button" id="btn-change-pwd" data-uid="<?php echo $attributes['uid'];?>"><?php _e('Change Password','jde-admin');?></button>
        </div>
        <div>
            <?php // do_action( 'wpml_footer_language_selector');  ?>

            <?php if ( ICL_LANGUAGE_CODE == 'en') : ?>

                <div class="wpml-ls-statics-footer wpml-ls wpml-ls-legacy-list-horizontal">
                    <ul><li class="wpml-ls-slot-footer wpml-ls-item wpml-ls-item-zh-hans wpml-ls-first-item wpml-ls-item-legacy-list-horizontal">
                            <a href="javascript:void();" class="wpml-ls-link"><img class="wpml-ls-flag" src="http://weixin.summergate.cn/wp-content/plugins/sitepress-multilingual-cms/res/flags/zh-hans.png" alt="zh-hans" title="简体中文"><span class="wpml-ls-native">简体中文</span><span class="wpml-ls-display"><span class="wpml-ls-bracket"> (</span>Chinese (Simplified)<span class="wpml-ls-bracket">)</span></span></a>
                        </li><li class="wpml-ls-slot-footer wpml-ls-item wpml-ls-item-en wpml-ls-current-language wpml-ls-last-item wpml-ls-item-legacy-list-horizontal">
                            <a href="javascript:void();" class="wpml-ls-link"><img class="wpml-ls-flag" src="http://weixin.summergate.cn/wp-content/plugins/sitepress-multilingual-cms/res/flags/en.png" alt="en" title="English"><span class="wpml-ls-native">English</span></a>
                        </li></ul>
                </div>

            <?php else : ?>
                <div class="wpml-ls-statics-footer wpml-ls wpml-ls-legacy-list-horizontal">
                    <ul><li class="wpml-ls-slot-footer wpml-ls-item wpml-ls-item-zh-hans wpml-ls-first-item wpml-ls-item-legacy-list-horizontal">
                            <a href="javascript:void();" class="wpml-ls-link"><img class="wpml-ls-flag" src="http://weixin.summergate.cn/wp-content/plugins/sitepress-multilingual-cms/res/flags/zh-hans.png" alt="zh-hans" title="简体中文"><span class="wpml-ls-native">简体中文</span><span class="wpml-ls-display"><span class="wpml-ls-bracket"> (</span>Chinese (Simplified)<span class="wpml-ls-bracket">)</span></span></a>
                        </li><li class="wpml-ls-slot-footer wpml-ls-item wpml-ls-item-en wpml-ls-current-language wpml-ls-last-item wpml-ls-item-legacy-list-horizontal">
                            <a href="javascript:void();" class="wpml-ls-link"><img class="wpml-ls-flag" src="http://weixin.summergate.cn/wp-content/plugins/sitepress-multilingual-cms/res/flags/en.png" alt="en" title="English"><span class="wpml-ls-native">English</span></a>
                        </li></ul>
                </div>
           <?php endif; ?>

        </div>

        <div class="admin-add-jde-user-popup">
            <span class="close-jde-popup"> <i class="fa fa-times-circle"></i></span>
            <label ><?php  _e('Old Password:','jde-login') ?><br><input type="text" name="old_pwd"> </label><br>
            <label ><?php  _e('New Password:','jde-login') ?><br><input type="text" name="new_pwd"> </label><br> <br>
            <button class="jde-button" id="btn-change-password"><?php  _e('SUBMIT','jde-login') ?></button>
        </div>

    </div>
<?php endif; ?>
