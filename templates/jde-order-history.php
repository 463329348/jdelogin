
<?php if ( !empty( $attributes['redirect'] ))  : ?>
<meta http-equiv="refresh" content="0; URL='<?php echo $attributes['redirect'];?>'" />

    <?php else : ?>


<div class="purchaser-home-container" style="height: 100vh;">

    <div class="jde-row title" style="background-color: #d3d3d3;">
        <div class="jde-col-sm-4  new-order order-op"><a href="<?php echo home_url() . '/purchaser-home-cross'; ?>"><?php  _e('New Order','jde-login'); ?></a></div>
        <div class="jde-col-sm-4  current-order order-op"><a href="<?php echo home_url() . '/jde-current-order'; ?>"><?php  _e('Current Order','jde-login'); ?></a></div>
        <div class="jde-col-sm-4  order-history order-op selected"><?php  _e('Order History','jde-login'); ?></div>
    </div>

    <div class="history-order-list">

        <?php if ( empty($attributes['orders'])) : ?>
                <span> <?php _e('No any history orders','jde-order');?> </span>
        <?php else :?>

        <table style="table-layout: fixed;" class="pending-order-table">
            <thead style="font-size: 14px;">
            <th><?php _e('ORDER NO.','jde-order');?></th>
            <th><?php _e('DATE','jde-order');?></th>
            <th><?php _e('STATUS','jde-order');?></th>
            <?php if ($attributes['show_price'] ) : ?>
            <th><?php _e('TOTAL','jde-order');?></th>
            <?php endif; ?>
            <th><?php _e('OP','jde-order');?></th>
            </thead>
            <tbody>

            <?php foreach  ( $attributes['orders']  as $order ) : ?>
                <?php $index = 1; ?>
                <tr class="order-line" id="order-line-<?php echo $order['no']; ?>">
                    <td><i class="fa fa-sort-down"></i> &nbsp; <?php echo $order['no'] ;?></td>
                    <td style="font-size: 14px;"><?php echo $order['date'] ;?><br></td>
                    <td style="font-size: 14px;"><?php echo $order['status'] ;?><br></td>
        <?php if ($attributes['show_price'] ) : ?>
                    <td><?php echo get_woocommerce_currency_symbol() . number_format($order['total_price'],2) ;?></td>
        <?php endif; ?>
                    <td>
                        <button data-oid="<?php echo $order['no'] ;?>" class="btn-order-clone"><?php _e('Clone','jde-order');?></button>
                    </td>
                </tr>
                <tr class="order-details-line">
        <?php if ($attributes['show_price'] ) : ?>
                    <td class="details" colspan="5">
                        <?php else: ?>
                    <td class="details" colspan="4">
            <?php endif; ?>
                        <div class="order-detail-topbar">
                            <span><?php _e('ORDER BY','jde-order');?> : <?php echo $order['opname'] ;?></span><br>
                        <span><?php _e('Remark','jde-order');?> : <?php echo $order['note'];?>
                        </span>
                        </div>
                        <div class="order-details-content">
                            <table>
                                <thead>
                                <th><?php _e('ITEM','jde-order');?></th>
                                <th><?php _e('CODE','jde-order');?></th>
                                <th><?php _e('QTY','jde-order');?></th>
        <?php if ($attributes['show_price'] ) : ?>
                                <th><?php _e('PRICE','jde-order');?></th>
                                <th><?php _e('TOTAL','jde-order');?></th>
            <?php endif; ?>
                                <th></th>
                                </thead>
                                <tbody>
                                <?php foreach ( $order['lines'] as $line ): ?>
                                    <tr id="order-item-<?php echo $order['id'] . '-' . $line['id']; ?>" data-oid="<?php echo $order['id'];?>" data-lineid="<?php echo $line['id'];?>">
                                        <td><?php echo $index++;?></td>
                                        <td><?php echo $line['code'];?></td>
                                        <td>
                                                <?php echo $line['qty'];?>

                                        </td>
              <?php if ($attributes['show_price'] ) : ?>
                                        <td>
                                            <?php echo get_woocommerce_currency_symbol() . number_format($line['price'],2);?>
                                        </td>
                                        <td><?php echo get_woocommerce_currency_symbol() . number_format($line['subtotal'],2);?></td>
               <?php endif; ?>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="promotion"><td class="promotion-box" colspan="6"><?php _e('Status:','jde-order');?> <?php echo $line['status'];?> | <?php _e('Promotion:','jde-order');?> <?php echo $line['promotion'];?></td></tr>

                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <div class="loading-more-flag" style="display:none;"><img src="<?php echo get_stylesheet_directory_uri() . '/images/loading_more.gif';?>"> </div>

        <?php endif; ?>
    </div>

    <input type="hidden" id="order-history-page-index" name="order-history-page-index" value="1">



</div>


<?php endif; ?>