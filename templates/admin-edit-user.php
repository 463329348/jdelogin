<?php if ( !empty( $attributes['redirect'] ))  : ?>
    <meta http-equiv="refresh" content="0; URL='<?php echo $attributes['redirect'];?>'" />


<?php else: ?>



<div class="admin-edit-user-container">

    <?php if ( $attributes['can_edit_users'] ) : ?>

<!--
    <div class="search-order-user">
        <div class="site-search" style="display: block;">
            <?php the_widget( 'WC_Widget_Product_Search', 'title=' ); ?>
        </div>
    </div>
-->
    <div style="text-align: center;margin-top: 20px;"><span><?php _e('VIEW USER & EDIT','jde-admin');?></span></div>

    <div class="admin-edit-user-topbar">
        <ul>
           <!-- <li>GROUP1(4)</li>
            <li>GROUP2(4)</li> -->
          <!--  <li style="float: left;margin-right: 10px;background-color: white">GROUP3(4)</li> -->
            <li class="admin-add-jde-user-btn"><a id="add-new"  href="#"><?php _e('Add New','jde-admin');?></a></li>
        </ul>
    </div>

    <div class="admin-add-jde-user-popup">
        <span class="close-jde-popup"> <i class="fa fa-times-circle"></i></span>
      <label> <?php  _e('Name:','jde-login') ?><br><input type="text" name="uname"> </label><br>
        <label ><?php  _e('Password:','jde-login') ?><br><input type="text" name="upassword"> </label><br>
        <label><?php  _e('Group:','jde-login') ?><br><select name="ugroup">
                <?php foreach ( $attributes['roles'] as $role ) : ?>
                    <option value="<?php echo $role; ?>"><?php echo $role; ?></option>
                <?php endforeach; ?>
            </select> </label><br>
        <label ><?php  _e('Customer:','jde-login') ?><br><select id="ulist-customers" name="ucustomer">
                <?php foreach ( $attributes['customers'] as $cid => $customer ) : ?>
                    <option value="<?php echo $cid; ?>"><?php echo '['. $cid .']' .$customer['c']; ?></option>
                <?php endforeach; ?>
            </select></label><br>

        <div class="jde-selected-customer">
        </div>
        <input type="hidden" name="userid" value="0">
        <button class="admin-add-jde-user-submit"><?php  _e('SUBMIT','jde-login') ?></button>
        <button style="display: none;" class="admin-add-jde-user-unbind"><?php  _e('UNBIND','jde-login') ?></button>
    </div>

    <div class="admin-edit-user-list" >
        <table>
            <thead><tr>
                <th><?php  _e('USER','jde-login') ?></th>
                <th><?php  _e('CUSTOMER','jde-login') ?></th>
                <th><?php  _e('GROUP','jde-login') ?></th>
                <th><?php  _e('EDIT','jde-login') ?></th>
            </tr></thead>
            <tbody>
            <?php foreach ( $attributes['users'] as $user ) : ?>
            <tr>
                <td><?php echo $user['name'];?></td>
                <td><?php echo $user['customer'];?></td>
                <td><?php echo $user['group'];?></td>
                <td class="edit-jde-user" data-username="<?php echo $user['name'];?>" data-group="<?php echo $user['group'];?>"
                    data-userid="<?php echo $user['id'];?>" data-bound="<?php echo $user['bound']; ?>"
                    data-cid="<?php echo $user['customer_id'];?>" data-cids="<?php echo $user['cids']; ?>"> <button><?php  _e('Edit','jde-login') ?></button></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
        <?php else : ?>
        <span> <?php _e('Sorry you can not edit users','jde-admin');?>  </span>
    <?php endif; ?>

</div>

<?php endif; ?>