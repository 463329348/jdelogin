<?php if ( !empty( $attributes['redirect'] ))  : ?>
    <meta http-equiv="refresh" content="0; URL='<?php echo $attributes['redirect'];?>'" />

    <?php else : ?>



<div class="jde-cart-container" style="height: 100vh;">

    <div class="jde-row-ttl" style="display: block; background-color: #d3d3d3;width: 100%;text-align: center;">
        <span><?php _e('My  Cart','jde-login'); ?></span>
    </div>

    <div class="jde-row jde-list-products" id="jde-cart-plist">
        <?php if ( empty($attributes['products'])) : ?>
            <span style="line-height: 80px;"> <?php _e('Sorry,Your Cart is empty!','jde-login'); ?> </span>
        <?php else: ?>
        <?php foreach ( $attributes['products'] as $k =>  $product ) : ?>
        <div class="jde-cart-product-box" data-sku="<?php echo $product['sku']; ?>" >
            <div class="product-image">
                <div class="product-index"> <span> <?php echo $k+1 . '.'; ?> </span></div>
                <div class="product-thumbnail">
                    <div class="thumbnail"><?php echo $product['thumbnail']; ?></div>
                    <div class="product-remove" data-link="<?php echo $product['remove_link']; ?>"> <?php _e('Del','jde-login'); ?> </div>
                </div>

            </div>
            <div class="product-details">
                <ul class="product-atts">
                    <li><?php echo $product['name']; ?></li>
                    <li><?php echo $product['country']; ?></li>
                </ul>
                <?php if ( $attributes['show_price'] ) : ?>
                <ul class="unit-price-zone">
                    <li><?php _e('UNIT PRICE','jde-shopping'); ?></li>
                    <li class="unit-price"><?php echo get_woocommerce_currency_symbol() . number_format($product['price'],2); ?></li>
                </ul>
                    <?php endif; ?>
            </div>
            <div class="product-buttons">
                <div class="ttl"><span><?php _e('QTY','jde-login'); ?></span></div>
                <?php
                $min_unit = $product['min_unit'];
                if ( empty($min_unit) ) $min_unit = 1;
                ?>

                <div class="product-buttons-zone" data-unit="<?php echo $min_unit; ?>" data-sign="<?php echo get_woocommerce_currency_symbol();?>" data-price="<?php echo $product['price']; ?>" data-key="<?php echo $product['cart_item_key']; ?>">
                    <div class="add"><i class="fa fa-plus"></i></div>
                    <div class="qty"><input class="qty-input" type="text" name="qty" value="<?php echo $product['qty']; ?>" disabled></div>
                    <div class="minus"><i class="fa fa-minus"></i></div>
                </div>
                <?php if ( $attributes['show_price'] ) : ?>
                <div class="product-subtotal-zone">
                    <ul>
                        <li><?php _e('SUB-TOTAL:','jde-shopping'); ?></li>
                        <li class="subtotal"><?php echo get_woocommerce_currency_symbol() . number_format($product['subtotal'],2); ?></li>
                    </ul>

                </div>
                <?php endif; ?>
            </div>
        </div>
        <?php endforeach; ?>
        <?php endif; ?>

    </div>
</div>

<div class="jde-shopping-cart-footer">

    <?php if ( $attributes['show_price'] ) : ?>

    <div style="background-color: white;"><span style="color: red;"><?php _e('Total:','jde-checkout');?> <?php echo get_woocommerce_currency_symbol() . number_format($attributes['total'],2)?></span></div>
    <?php endif; ?>
    <a class="jde-col-sm-6 new-product" href="<?php echo esc_url( home_url() . '/purchaser-home') ; ?>">
    <div >
        <span><?php _e('ADD NEW <br> PRODUCT','jde-shopping'); ?> </span>
    </div> </a>

    <a id="checkout-from-cart" class="jde-col-sm-6 checkout" href="<?php echo esc_url( home_url() . '/jde-checkout') ; ?>">
    <div >
        <span><?php _e('CHECK OUT','jde-shopping'); ?></span>
    </div></a>
</div>

<?php endif; ?>