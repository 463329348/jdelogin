<?php
/**
 * Plugin Name:       JDE Login
 * Description:       A plugin that replaces the WordPress login flow with a custom JDE Login page.
 * Version:           1.0.0
 * Author:            Michael Yue - Digital Jungle Agency
 * License:           GPL-2.0+
 * Text Domain:       jde-login
 */


class JDE_Login_Plugin {

    const LOADING_MORE_PRODUCTS_MAX = 8;
    const JDE_SYNC_DAILY_CRON_EVENT = 'jde_sync_daily_event';
    const JDE_NOTIFY_DAILY_CRON_EVENT = 'jde_notify_daily_event';
    const JDE_NOTIFY_2nd_DAILY_CRON_EVENT = 'jde_notify_2nd_daily_event';
    const JDE_NOTIFY_3rd_DAILY_CRON_EVENT = 'jde_notify_3rd_daily_event';
    const JDE_SCAN_EMAIL_CRON_EVENT = 'jde_scan_notify_email_event';
    const JDE_CRON_HALF_HOUR_RECURRENCE = 'jde_every_half_hour';
    const JDE_CRON_ONE_MINUTE_RECURRENCE = 'jde_every_one_minute';


    // 8080 port for testing, 8001 for product version
    const JDE_API_GET_ORDER_URL = 'http://58.32.233.46:8001/ditools/api/jdeOrderLinesIDs?orderId=';
    const JDE_API_GET_LINE_URL = 'http://58.32.233.46:8001/ditools/api/jdeOrderLineDetail?jdeLineNo=';

    /**
     * Initializes the plugin.
     *
     * To keep the initialization fast, only add filter and action
     * hooks in the constructor.
     */
    public function __construct() {

        // add short code for jde-login-form
        add_shortcode( 'custom-login-form', array( $this, 'render_login_form' ) );

        // add short code for admin access page
        add_shortcode( 'admin-access-form', array( $this, 'render_admin_access' ) );

        // add admin edit user form
        add_shortcode('admin-edit-user-form',array($this, 'render_admin_edit_user'));

        // add normal purchase home page form
        add_shortcode('jde-purchaser-home-form',array($this,'render_purchaser_home'));
        add_shortcode('jde-purchaser-home-cross-form',array($this,'render_purchaser_home_cross'));

        // add jde checkout form
        add_shortcode('jde-checkout-form',array($this,'render_jde_checkout'));

        // add admin setting/pending order/order history form
        add_shortcode('admin-setting-form',array($this, 'render_admin_setting'));
        add_shortcode('admin-pending-order-form',array($this, 'render_admin_pending_order'));
        add_shortcode('admin-order-history-form',array($this, 'render_admin_order_history'));
        add_shortcode('jde-contact-us-form',array($this, 'render_jde_contact_us'));
        add_shortcode('jde-user-setting-form',array($this, 'render_jde_user_setting'));

        // for jde shopping cart form
        add_shortcode('jde-shopping-cart',array($this,'render_jde_shopping_cart'));

        // for current order form and order history form
        add_shortcode('jde-current-order',array($this,'render_jde_current_order'));
        add_shortcode('jde-order-history',array($this,'render_jde_order_history'));

        // redirect all login to our jde login page
        add_action( 'login_form_login', array( $this, 'redirect_to_custom_login' ) );

        // once logout redirect to our login again
        add_action( 'wp_logout', array( $this, 'redirect_after_logout' ) );

        // try to handle authenticate error
        add_filter( 'authenticate', array( $this, 'jde_redirect_at_authenticate' ), 100, 3 );

        // once user login ok, redirect to our specified page
        add_filter( 'login_redirect', array( $this, 'redirect_after_login' ), 10, 3 );

        // set our cron recurrence
        add_filter( 'cron_schedules', array($this,'jde_add_cron_recurrence_interval'));
        add_filter( 'cron_schedules',array($this,'jde_add_cron_recurrence_interval_onemin'));

        // add jde cron job
        add_action( self::JDE_SYNC_DAILY_CRON_EVENT, array($this, 'sync_jde_orders_daily') );
        add_action( self::JDE_NOTIFY_DAILY_CRON_EVENT, array($this, 'notify_jde_orders_daily') );
        add_action( self::JDE_NOTIFY_2nd_DAILY_CRON_EVENT, array($this, 'notify_jde_orders_daily') );
        add_action( self::JDE_NOTIFY_3rd_DAILY_CRON_EVENT, array($this, 'notify_jde_orders_daily') );
        add_action( self::JDE_SCAN_EMAIL_CRON_EVENT , array($this,'scan_notify_email_check'));

        // for notify customer sales email asynchronous logic
        add_shortcode('order-notify-email',array($this,'render_order_notify_email'));

        // add executed order sending email
        add_shortcode('jde-execute-order-email',array($this,'execute_order_email'));

        add_action('wp_authenticate',array($this,'jde_authenticate'),20,2);

    }

    /**
     * @param $username
     * @param $password
     */
    public function jde_authenticate($username , $password){
        write_log('user : ' . $username . ' try to login now');
        $user = get_user_by('login',$username);
        if ( !empty($user) ) {
            include_once(get_stylesheet_directory().'/includes/jde-user.php');
            $jdeUser = new JdeUser();
            $cur_user = $jdeUser->get_by(['wp_user_id' => $user->ID] );
            if ( !empty($cur_user) ) {
                write_log('user ' . $username . 'has been bound with others');
                $error_codes = '';
                if ( isset($_POST['wxid']) ) {
                    if (  $_POST['wxid'] != $cur_user[0]->wx_openid ) {
                        $error_codes = 'bind_already';
                    }
                } else {
                    $error_codes = 'bind_already';
                }
                if ( isset($_POST['wxid']) ) {
                    $wxid = $_POST['wxid'];
                    $login_url = get_home_url() . '/member-login/?wxid=' . $wxid;
                } else {
                    $login_url = get_stylesheet_directory_uri() . '/wx.php?d=shop';
                }
                $login_url = add_query_arg( 'login', $error_codes, $login_url );
                wp_redirect( $login_url );
                exit;
            }
        }
    }

    /**
     * in case amdin change password or unbind the user
     * user need to re-login with name and password again
     * @return bool
     */
    private function shouldRebind(){
        // check to see if the current user still in jde-user table
        // if not , should re-login
        require_once(get_stylesheet_directory() . '/includes/jde-user.php');
        $jdeUser = new JdeUser();
        // once user has been unbound , user need to re-login again
        $user = $jdeUser->get_by(['wp_user_id' => get_current_user_id()]);
        if ( empty($user) ) {

            // logout current user - clear all related data
            wp_destroy_current_session();
            wp_clear_auth_cookie();

            return true;
        }
        return false;
    }

    /**
     * process sync jde orders issue
     * will be called by system based cron like :
     * /15 * * * wget -q -O - http://yourdomain.com/wp-cron.php?doing_wp_cron
     * refer to :
     * https://crontab-generator.org/
     * to write a cron easily
     */
    public function sync_jde_orders_daily(){

        write_log('JDE Cron ' . __FUNCTION__ . ' doing now');

        // try to get all needed to sync orders
        // Accepts a string: one of 'pending', 'processing', 'on-hold', 'completed',
        // 'refunded, 'failed', 'cancelled', or a custom order status. [ executed, submitted]
        $page_index = 1;
        $args = array(
            'status' => array('on-hold','pending','processing','executed','submitted'),
            'limit' => 20,
            'page' => 1
        );

        $wcOrders = wc_get_orders($args);
        while ( !empty($wcOrders) ) {
            include_once(get_stylesheet_directory() . '/includes/jde-utils.php');
            foreach ($wcOrders as $order) {
                $orderId = $order->get_id();
                //   $orderId = 2;  // test only
                $response = wp_remote_get(self::JDE_API_GET_ORDER_URL . $orderId);
                if (is_array($response)) {
                    $code = wp_remote_retrieve_response_code($response);
                    if ($code == 200) {
                        $jdeOrderLines = json_decode(wp_remote_retrieve_body($response));

                        write_log('[API]' . __FUNCTION__ . ' sync ' . $orderId . ' returned order lines :  ' . print_r($jdeOrderLines, true));

                        foreach ($jdeOrderLines as $line) {
                            // try to get each jde order line id details
                            $lineResp = wp_remote_get(self::JDE_API_GET_LINE_URL . $line);
                            if (is_array($lineResp)) {
                                $lineRtCode = wp_remote_retrieve_response_code($lineResp);
                                if ($lineRtCode == 200) {
                                    $jdeLineDetails = json_decode(wp_remote_retrieve_body($lineResp));
                                    write_log('[API]' . __FUNCTION__ . ' sync ' . $orderId . ' returned order lines data :  ' . print_r($jdeLineDetails, true));
                                    $this->update_jde_order_line($order, $jdeLineDetails);
                                } else {
                                    write_log('[API]' . __FUNCTION__ . ' sync ' . $orderId . ' get line ' . $line . ' details failed: ' . print_r($lineResp, true));
                                }
                            } else {
                                write_log('[API]' . __FUNCTION__ . ' sync ' . $orderId . ' get line ' . $line . ' details failed: ' . print_r($lineResp, true));
                            }
                        }
                        // update order status
                        if (!empty($jdeOrderLines)) {
                            $this->refresh_order_status($order);
                        }

                    } else {
                        write_log('[API]' . __FUNCTION__ . ' sync ' . $orderId . ' failed: ' . print_r($response, true));
                    }

                } else {
                    write_log('[API]' . __FUNCTION__ . ' sync ' . $orderId . ' failed: ' . print_r($response, true));
                }

            }
            write_log('JDE Cron ' . __FUNCTION__ . ' done for page: ' . $page_index);
            $page_index++;
            $args = array(
                'status' => array('on-hold','pending','processing','executed','submitted'),
                'limit' => 20,
                'page' => $page_index
            );

            $wcOrders = wc_get_orders($args);
        }
        // try to get order
        write_log('JDE Cron ' . __FUNCTION__ . ' done this time');
    }

    /**
     * use cron to scan what emails we should try to send
     * will check in the our table wp-jde-order-email-notify
     */
    public function scan_notify_email_check(){

      //  write_log('JDE Cron ' . __FUNCTION__ . ' doing now');
        $is_doing = get_option('jde_email_notify_checking');
        if ( !empty($is_doing) ) return;

        add_option('jde_email_notify_checking','doing');

        include_once(get_stylesheet_directory().'/includes/jde-email-notify.php');
        include_once(get_stylesheet_directory().'/includes/jde-order.php');

        $notifies_obj = new JdeEmailNotify();
        $notifies = $notifies_obj->getNotifies();
        if ( !empty($notifies) ) {
            $jdeOrderObj = new JdeOrder();
            foreach ($notifies as $id => $data ) {
                if ( $data['type'] == JdeEmailNotify::TYPE_NOTIFY_CUSTOMER ) {
                    // call notify to sales person
                    $jdeOrderObj->emailOrderNotifyToSales($data['order_id']);
                } else if ( $data['type'] == JdeEmailNotify::TYPE_NOTIFY_MANAGER ) {
                    // notify customer this order
                    $jdeOrderObj->notifyCustomerOrderChangedByEmail($data['order_id']);
                }
                $notifies_obj->deleteNotify($id);
            }
        }
        delete_option('jde_email_notify_checking');
       // write_log('JDE Cron ' . __FUNCTION__ . ' done this time');
    }

    /**
     * process notify jde orders issue
     * will be called by system based cron like :
     * [*]/30 * * * * wget -q -O - http://weixin.summergate.cn/wp-cron.php?doing_wp_cron
     * refer to :
     * https://crontab-generator.org/
     * to write a cron easily
     */
    public function notify_jde_orders_daily(){

        write_log('JDE Cron ' . __FUNCTION__ . ' doing now');
        write_log('anyway sync with JDE again');

        $this->sync_jde_orders_daily();

        require_once(get_stylesheet_directory()  . '/includes/jde-order.php');
        $jdeOrderObj = new JdeOrder();
        $orderFile = $jdeOrderObj->exportCsvOrders();

        // send file to email , currently default as :  WeChatSH@summergate.com
        if ( !empty($orderFile) ) {

            $is_test = get_option('is-jde-test');
            if ( !empty($is_test) ) {
                $to = 'Test09.SG@summergate.com';
            } else {
                $to = 'WeChat.SH@summergate.com';
            }
            // $to = 'michael.yue@digitaljungle.agency';
            //$headers = array();
            $headers = 'From: Wechat <michaeyue@summergate.com>' . "\r\n";
            $attachments = array( get_stylesheet_directory() . '/'.$orderFile);
           // $subject = '[WxOrders]-' . date(' Y-m-d h:i:s') .'-' . get_current_user_id();
            $subject = '[WxOrders]-' . current_time('mysql') .'-' . get_current_user_id();
            $message = __('JDE WeChat Exported Orders, please check the attachment','jde-email');
            $rt = wp_mail($to, $subject, $message, $headers, $attachments);
            if ( $rt ) {
                write_log('Orders has been send to your email successfully!');
            } else {
                write_log('Failed to send exported order to your email',1);
            }
        } else {
            write_log('Failed to export orders!',1);
        }

        // try to get order
        write_log('JDE Cron ' . __FUNCTION__ . ' done this time');
    }

    /**
     * if all delivered , we set order as completed status
     * @param $order
     */
    private function refresh_order_status($order){
        $status = 'submitted';
        $order_items  = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );


        /**
         *  $status = 'Submitted';
            $status = 'On-hold';
            $status = 'Completed';
            $status = 'Prepared';
            $status = 'Delivered';
            $status = 'Ready-delivery';
        }
         */
        $statusNums = array('Delivered' => 0,'On-hold' => 0, 'Completed' => 0);
        $itemNums = count($order_items);
        foreach ( $order_items as $item ) {
            $istatus = $item->get_meta('status');
            if ( isset($statusNums[$istatus]) ) {
                $statusNums[$istatus]++;
            }
        }
        if ( $statusNums['Delivered'] == $itemNums ) {
            $status = 'completed';
        }

        if ( $statusNums['On-hold'] == $itemNums ) {
            $status = 'cancelled';
        }

        if ( $statusNums['Completed'] == $itemNums ) {
            $status = 'completed';
        }

        $order->update_status($status);
        $order->save();

    }

    /**
     * @param $order
     * @param $line
     */
    public function update_jde_order_line($order,$line){
        if ( isset($line->order_line_id) && $line->order_line_id > 0 ) {
            $item = $order->get_item($line->order_line_id);
            if ( !empty($item) ) {

                // check to see if we should insert a new line into currently order
                // $oldPrice = $jdeOrder->get_item_subtotal($item);
                $price = 0;
                if ( isset($line->price) && !empty($line->price) ) {
                    $price = floatval($line->price);
                }

                // flag for split or update item
                $updateItems = false;

                // based on price is not so good , so we based on jde line id to check split item or not
                $jdeLineId = isset( $line->jde_line_id ) ? $line->jde_line_id : '';
                $jdeOrderId = isset( $line->jde_order_id ) ? $line->jde_order_id : '';

                // not sync with jde before just update it
                // jde line id with '*.*' format must be split line
                if ( !empty($jdeLineId) && strpos($jdeLineId,'.') == FALSE ) {
                    $updateItems = true;
                } else {
                    // get all lines now
                    $all_items = $order->get_items();
                    $joinOidLid = $jdeOrderId . '.' . $jdeLineId;
                    foreach ($all_items as $hitem) {
                        $jde_oid_lineid = $hitem->get_meta('jde_order_id') . '.' . $hitem->get_meta('jde_line_id');
                        if ( $jde_oid_lineid == $joinOidLid ) {
                            $updateItems = true;
                            $item = $hitem;
                            break;
                        }
                    }
                }

                if ( $updateItems ) // just update current one
                {
                    // update promotion code in case
                    if ( isset($line->promotion_code) && !empty($line->promotion_code) ) {
                        $item->update_meta_data('promotion',$line->promotion_code);
                    }

                    // update order line status
                    // jde returned status code , we need map them into our status name as below:
                    // jde order line status as :
                    // pending , preparing , shipped
                    if ( isset($line->status) && !empty($line->status) ) {
                        $item->update_meta_data('status',JdeUtils::get_wpstatus_by_jdecode($line->status));
                    } else {
                        // set all status as submitted
                        $item->update_meta_data('status','Submitted');
                    }

                    // update order line JDE line id
                    if ( !empty($jdeLineId) ) {
                        $item->update_meta_data('jde_line_id',$jdeLineId);
                    }
                    if ( !empty($jdeOrderId) ) {
                        $item->update_meta_data('jde_order_id',$jdeOrderId);
                    }

                    // update order line quantity and price
                    $qty = 0;
                    if ( isset($line->qty) && !empty($line->qty) ) {
                        $qty = intval($line->qty);
                    }

                    if (  $qty > 0 ) {
                        $default_args = array(
                            'subtotal'     => $qty * $price,
                            'total'        => $qty * $price,
                            'quantity'     => $qty,
                        );
                        $item->set_props($default_args);
                    }
                    $item->save();

                } else {

                    // in case split an order item
                    // based on sku get new product id in wx system
                    $sku = isset($line->sku) ? $line->sku : '';
                    $newProductId = wc_get_product_id_by_sku($sku);
                    $newQty = isset($line->qty) ? intval($line->qty) : 0;
                    $newProduct = wc_get_product($newProductId);
                    if ( !empty($newProduct) && $newQty > 0 ) {
                        $itemid = $order->add_product($newProduct,$newQty);
                        $newItem = $order->get_item($itemid);
                        $newItem->update_meta_data('status','new');
                        $newItem->update_meta_data('promotion',$line->promotion_code);
                        $newItem->update_meta_data('jde_line_id',$jdeLineId);
                        $newItem->update_meta_data('jde_order_id',$jdeOrderId);
                        // update price here , price maybe changed
                        $default_args = array(
                            'subtotal'     => $newQty * $price,
                            'total'        => $newQty * $price,
                            'quantity'     => $newQty,
                        );
                        $item->set_props($default_args);
                        $newItem->save();
                    } else {
                        write_log('[API] ' . __FUNCTION__ . ' add split line failed: ' . print_r($line,true)  ,1);
                    }
                }
            } else {
                write_log('[API] ' . __FUNCTION__ . 'order id:' .$order->get_id() . ' item id:' .$line->order_line_id . ' not found'  ,1);
            }
        } else {
            write_log('[API] ' . __FUNCTION__ . 'order id:' .$order->get_id() . ' item invalid' .print_r($line,true)  ,1);
        }
    }

    /**
     * Returns the URL to which the user should be redirected after the (successful) login.
     *
     * @param string           $redirect_to           The redirect destination URL.
     * @param string           $requested_redirect_to The requested redirect destination URL passed as a parameter.
     * @param WP_User|WP_Error $user                  WP_User object if login was successful, WP_Error object otherwise.
     *
     * @return string Redirect URL
     */
    public function redirect_after_login( $redirect_to, $requested_redirect_to, $user ) {

         $redirect_url = get_home_url() .  '/member-login' ;

        write_log('redirect_after_login called');

        if ( !isset( $user->ID ) ) {
            write_log('no user login');
           // wp_validate_redirect( $redirect_url, home_url() );
            return $redirect_url;
        }

        if ( user_can( $user, 'manage_options' ) ) {
            // Use the redirect_to parameter if one is set, otherwise redirect to admin dashboard.
            if ( $requested_redirect_to == '' ) {
                $redirect_url = admin_url();
            } else {
                $redirect_url = $requested_redirect_to;
            }
        } else {

            // based on uer role go to summergate home page
            include_once(get_stylesheet_directory().'/includes/jde-internal-user.php');

            // get try to get user role
            $jdeInternalUserObj = new JdeInternalUser();
            $jdeInternalUser = $jdeInternalUserObj->get_by(['wp_user_id' => $user->ID] );
            $role = JdeInternalUser::JDE_USER_ROLE_BIZ_PURCHASER;
            if ( !empty($jdeInternalUser) ) {
                $jdeInternalUser = reset($jdeInternalUser);
                $role = $jdeInternalUser->user_group;
                if ( empty($role) ) {
                    $role = JdeInternalUser::JDE_USER_ROLE_BIZ_PURCHASER;
                }
            }

            $redirect_url = get_home_url();
            switch ( $role ) {
                case JdeInternalUser::JDE_USER_ROLE_BIZ_OWNER:
                    $redirect_url .=  '/purchaser-home-cross';
                    break;

                case JdeInternalUser::JDE_USER_ROLE_SALES_ASSISTANT:
                    $redirect_url .=  '/purchaser-home';
                    break;

                case JdeInternalUser::JDE_USR_ROLE_IT_MANAGER:
                    $redirect_url .= '/admin-access';
                    break;

                case JdeInternalUser::JDE_USER_ROLE_BIZ_PURCHASER:
                default:
                    $redirect_url .=  '/purchaser-home-cross';
                    break;
            }

            // if wechat user not bound with JDE account
            // do it now
            if ( isset($_POST['wxid']) || isset($_GET['wxid'])) {
                $wxid = isset($_POST['wxid']) ? $_POST['wxid'] : ( isset($_GET['wxid']) ? $_GET['wxid'] : '');
                write_log('user login with wxid : ' . $wxid);

                if ( !empty($wxid) ) {
                    $jdeUserId = $this->bindWithWxAccount($user->ID, $wxid);
                    write_log('bound wx userid :' . $jdeUserId);
                    if ( $jdeUserId <= 0 ) {

                        // bind failed , just redirect to login page again
                        // based on uer role go to summergate home page
                        $redirect_url = get_home_url() . '/member-login/?wxid=' . $wxid . '&out=true&login=bind_already';

                        // user has been bound with others people we should not logout others session
                        // so comment the following lines
                        /*
                        if ( is_user_logged_in() ) {

                            write_log('after login but should logout now : redirect ' . $redirect_url);

                            // we should log out the person
                            wp_logout();
                           // wp_set_current_user(0);
                            write_log('after login but should logout now - 2 - relogin from wx');
                           // header("Location: " . get_home_url() . '/member-login/?wxid=' . $wxid . '&login=bind_already');
                           // exit();
                        }
                        */
                    }
                }
            }
        }
        write_log('after login redirect end redirect: ' . $redirect_url);
        return wp_validate_redirect( $redirect_url, home_url() );
    }

    /**
     * @param $jdeUserId
     * @param $wxUserId
     * @return InsertQuery|int
     */
    private function bindWithWxAccount($jdeUserId,$wxUserId){
        include_once(get_stylesheet_directory().'/includes/jde-user.php');
        $jdeUser = new JdeUser();
        return  $jdeUser->addUser($jdeUserId,$wxUserId);
    }

    /**
     * Redirect to custom login page after the user has been logged out.
     */
    public function redirect_after_logout() {
        $redirect_url = get_stylesheet_directory_uri() . '/wx.php?d=shop';
      //  $redirect_url = home_url( 'member-login?logged_out=true' );
        wp_safe_redirect( $redirect_url );
        exit;
    }

    /**
     * Finds and returns a matching error message for the given error code.
     *
     * @param string $error_code    The error code to look up.
     *
     * @return string               An error message.
     */
    private function get_error_message( $error_code ) {
        switch ( $error_code ) {
            case 'bind_already':
                return __( 'The account has been bound', 'jde-login' );

            case 'empty_username':
                return __( 'You do have an username, right?', 'jde-login' );

            case 'empty_password':
                return __( 'You need to enter a password to login.', 'jde-login' );

            case 'invalid_username':
            case 'invalid_email':
                return __(
                    "We don't have any users with that email address. Maybe you used a different one when signing up?",
                    'jde-login'
                );

            case 'incorrect_password':
                $err = __(
                    "The username or password you entered wasn't quite right.",
                    'jde-login'
                );
                return sprintf( $err, wp_lostpassword_url() );

            default:
                break;
        }

        return __( 'An unknown error occurred. Please try again later.', 'personalize-login' );
    }

    /**
     * Redirect the user after authentication if there were any errors.
     *
     * @param Wp_User|Wp_Error  $user       The signed in user, or the errors that have occurred during login.
     * @param string            $username   The user name used to log in.
     * @param string            $password   The password used to log in.
     *
     * @return Wp_User|Wp_Error The logged in user, or error information if there were errors.
     */
    function jde_redirect_at_authenticate( $user, $username, $password ) {
        // Check if the earlier authenticate filter (most likely,
        // the default WordPress authentication) functions have found errors
        if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
            if ( is_wp_error( $user ) ) {
                $error_codes = join( ',', $user->get_error_codes() );
                if ( isset($_POST['wxid']) ) {
                    $wxid = $_POST['wxid'];
                    $login_url = get_home_url() . '/member-login/?wxid=' . $wxid;
                } else {
                    $login_url = home_url('member-login');
                }
                $login_url = add_query_arg( 'login', $error_codes, $login_url );

                wp_redirect( $login_url );
                exit;
            } else {
                // check to see if user has been bound with others
                $check_user = get_user_by('login',$username);
                if ( !empty($check_user) ) {
                    include_once(get_stylesheet_directory().'/includes/jde-user.php');
                    $jdeUser = new JdeUser();
                    $cur_user = $jdeUser->get_by(['wp_user_id' => $check_user->ID] );
                    if ( !empty($cur_user) ) {
                        write_log('user ' . $username . 'has been bound with others');
                        $error_codes = '';
                        if ( isset($_POST['wxid']) ) {
                            if (  $_POST['wxid'] != $cur_user[0]->wx_openid ) {
                                $error_codes = 'bind_already';
                            }
                        } else {
                            $error_codes = 'bind_already';
                        }
                        if ( isset($_POST['wxid']) ) {
                            $wxid = $_POST['wxid'];
                            $login_url = get_home_url() . '/member-login/?wxid=' . $wxid;
                        } else {
                            $login_url = home_url('member-login');
                        }
                        $login_url = add_query_arg( 'login', $error_codes, $login_url );
                        wp_redirect( $login_url );
                        exit;
                    }
                }
            }
        }

        return $user;
    }


    /**
     * Redirect the user to the custom login page instead of wp-login.php.
     */
    function redirect_to_custom_login() {
        if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
            $redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : null;
            if ( is_user_logged_in() ) {
                $this->redirect_logged_in_user( $redirect_to );
                exit;
            }

            // The rest are redirected to the login page
            $login_url = home_url( 'member-login' );
            if ( !empty( $redirect_to ) ) {
                $login_url = add_query_arg( 'redirect_to', $redirect_to, $login_url );
            }

            wp_redirect( $login_url );
            exit;
        }
    }

    /**
     * Redirects the user to the correct page depending on whether he / she
     * is an admin or not.
     *
     * @param string $redirect_to   An optional redirect_to URL for admin users
     */
    private function redirect_logged_in_user( $redirect_to = null ) {
        $user = wp_get_current_user();
        if ( user_can( $user, 'manage_options' ) ) {
            if ( $redirect_to ) {
                wp_safe_redirect( $redirect_to );
            } else {
                wp_redirect( admin_url() );
            }
        } else {
            wp_redirect( home_url( 'purchaser-home' ) );
        }
    }

    /**
     * try to get member-login url
     * in case unbind, we need append wx_openid as a parameter
     * @return string
     */
    private function getMemberloginUrl(){
        /*$wx_openid = get_option('wx-jde-unbind-'.get_current_user_id());
        if ( !empty($wx_openid) ) {
            $url  = esc_url(home_url() . '/member-login/?wxid=' . $wx_openid);
        } else {
            $url  = esc_url(home_url() . '/member-login');
        }*/
        $url = get_stylesheet_directory_uri() . '/wx.php?d=shop';
        return $url;
    }

    /**
     * @param $attributes
     * @param null $content
     * @throws Exception
     */
    public function render_order_notify_email($attributes, $content = null){

        $order_id = isset($_POST['oid']) ? $_POST['oid'] : 0;
        write_log('render_order_notify_email called for order : ' . $order_id);

        if ( $order_id > 0 ) {
            $order = wc_get_order($order_id);
            if ( !empty($order) ) {
                $op_id = $order->get_customer_id();
                include_once(get_stylesheet_directory().'/includes/jde-internal-user.php');
                include_once(get_stylesheet_directory().'/includes/jde-customer.php');
                $userObj = new JdeInternalUser();
                $user = $userObj->get_by(['wp_user_id' => $op_id]);
                if ( !empty($user) ) {
                    $user = reset($user);
                    $customerObj = new JdeCustomer();
                    $customer = $customerObj->get_by(['jde_customer_id' => $user->jde_customer_id]);
                    if ( !empty($customer) ) {
                        $customer = reset($customer);
                        $notify_email = $customer->notify_email;
                        if ( empty($notify_email) ) {

                            $is_test = get_option('is-jde-test');
                            if ( !empty($is_test) ) {
                                $notify_email = 'Test09.SG@summergate.com';
                            } else {
                                $notify_email = 'WeChat.SH@summergate.com';
                            }
                        }
                        $headers = 'From: Wechat <michaeyue@summergate.com>' . "\r\n";
                        $subject = '#'. $order->get_id() . ' '. __( 'New Order Added ', 'jde-email');
                        $message = '#'. $order->get_id() . ' ' . __('New Order Added','jde-email');
                        $rt = wp_mail($notify_email, $subject, $message, $headers);
                        if ( $rt ) {
                            write_log('send new order notify email : ' . $notify_email . ' msg: done successfully ');
                        } else {
                            write_log('send new order notify email : ' . $notify_email . ' msg: ' . $message,1);
                        }
                    }
                }
            }
        }
        echo 'done';
    }

    /**
     * @param $attributes
     * @param null $content
     * @return string
     */
    public function render_jde_contact_us($attributes, $content = null){
        // Parse short code attributes
        $default_attributes = array();// array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );

        $attributes['redirect'] = '';
        if ( !is_user_logged_in() || $this->shouldRebind() ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        } else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }

        }

        return $this->get_template_html( 'jde-contact-us', $attributes );
    }

    /**
     * @param $attributes
     * @param null $content
     * @return string
     */
    public function render_jde_user_setting($attributes, $content = null){
        // Parse short code attributes
        $default_attributes = array();// array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );

        $attributes['redirect'] = '';
        $attributes['user_name'] = '';
        $attributes['customer_name'] = '';
        $attributes['uid'] = '';

        if ( !is_user_logged_in()  || $this->shouldRebind()  ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        } else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }

            // get user name and standby customer name
            include_once(get_stylesheet_directory().'/includes/jde-internal-user.php');
            include_once(get_stylesheet_directory().'/includes/jde-customer.php');
            $userObj = new JdeInternalUser();
            $user = $userObj->get_by(['wp_user_id' => get_current_user_id()]);
            if ( !empty($user) ) {
                $user = reset($user);
                $attributes['user_name'] = $user->username;
                $attributes['uid'] = $user->id;
                $customerObj = new JdeCustomer();
                $customer = $customerObj->get_by(['jde_customer_id' => $user->jde_customer_id]);
                if ( !empty($customer) ) {
                    $customer = reset($customer);
                    // get user name based on current language
                    $attributes['customer_name'] = ( ICL_LANGUAGE_CODE == 'en') ? $customer->ename : $customer->cname;
                }
            }
        }

        return $this->get_template_html( 'jde-user-setting', $attributes );
    }

    /**
     * @param $attributes
     * @param null $content
     * @return string
     */
    public function render_jde_current_order($attributes,$content=null){
        // Parse short code attributes
        $default_attributes = array();// array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );

        $attributes['redirect'] = '';
        $attributes['orders'] = array();
        if ( !is_user_logged_in()  || $this->shouldRebind() ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        } else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }

            // get all processing orders
            require_once ( get_stylesheet_directory()  . '/includes/jde-order.php');
            $jdeOrderObj = new JdeOrder();
          //  $attributes['order'] = $jdeOrderObj->getLatestOrder();
            $attributes['orders'] = $jdeOrderObj->getCustomerProcessingOrders();
        }

        return $this->get_template_html( 'jde-current-order', $attributes );
    }

    /**
     * @param $attributes
     * @param null $content
     * @return string
     */
    public function render_jde_order_history($attributes,$content=null){
        // Parse short code attributes
        $default_attributes = array();// array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );

        $attributes['redirect'] = '';
        if ( !is_user_logged_in()  || $this->shouldRebind() ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        } else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }

            // check to see if the customer can see the price
            require_once ( get_stylesheet_directory()  . '/includes/jde-internal-user.php');
            $jdeInternalUserObj = new JdeInternalUser();
            $attributes['show_price'] = $jdeInternalUserObj->canSeePrice();

            // get all processing orders
            require_once ( get_stylesheet_directory()  . '/includes/jde-order.php');
            $jdeOrderObj = new JdeOrder();
            $attributes['orders'] = $jdeOrderObj->getCustomerHistoryOrders();

        }

        return $this->get_template_html( 'jde-order-history', $attributes );
    }

    /**
     * jde checkout form
     * @param $attributes
     * @param null $content
     * @return string
     */
    public function render_jde_checkout($attributes,$content=null){
        // Parse short code attributes
        $default_attributes = array();// array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );

        $attributes['redirect'] = '';
        if ( !is_user_logged_in()  || $this->shouldRebind() ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        } else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }

            // get shipping address
            require_once ( get_stylesheet_directory()  . '/includes/jde-user.php');
            require_once ( get_stylesheet_directory()  . '/includes/jde-internal-user.php');
            require_once ( get_stylesheet_directory()  . '/includes/jde-customer.php');
            require_once ( get_stylesheet_directory()  . '/includes/jde-customer-addr.php');
            require_once ( get_stylesheet_directory()  . '/includes/jde-customer-ref.php');
            $jdeUserObj = new JdeUser();
            $jdeCustomerRefObj = new JdeCustomerRef();
            $jdeInternalUserObj = new JdeInternalUser();
            $jdeInternalUser = $jdeInternalUserObj->get_by(['wp_user_id' => get_current_user_id() ]);
            if ( !empty($jdeInternalUser) ) {
                $custtomer_id = $jdeInternalUser[0]->jde_customer_id;
                // get customer addresses
                $jdeCustomerAddrObj = new JdeCustomerAddr();
                $addresses = $jdeCustomerAddrObj->get_by(['jde_customer_id' => $custtomer_id]);
                $attributes['addresses'] = array();
                foreach ( $addresses as $address ) {
                    $attributes['addresses'][] = array(
                        'id' => $address->id,
                        'telephone' => $address->telephone,
                        'address' => $address->state . ' ' . $address->city . ' ' . $address->suburb . ' ' . $address->address
                    );
                }

                // get operator name
                if ( !empty($jdeInternalUser) ) {
                    $attributes['op_name'] = $jdeInternalUser[0]->username;
                }
                $attributes['show_price'] = $jdeInternalUserObj->canSeePrice();

                // get customer name
                $jdeCustomerObj = new JdeCustomer();
                $jdeCustomer = $jdeCustomerObj->get_by(['jde_customer_id' => $custtomer_id]);
                if ( !empty($jdeCustomer) ) {
                    $attributes['customer_ename'] = $jdeCustomer[0]->ename;
                    $attributes['customer_cname'] = $jdeCustomer[0]->cname;
                    $attributes['note'] = $jdeCustomer[0]->note;
                }

            }

            // get all shopping products
            $attributes['products'] = array();
            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
                $product_sku = $_product->get_sku();
                $product_qty = $cart_item['quantity'];
                $product_name = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key);
                $product_price =  $_product->get_price();
                $product_subtotal = $product_price * $product_qty;

                $contract_price = $jdeCustomerRefObj->getCrossPrice($custtomer_id,$product_sku);
                if ( $contract_price > 0 ) {
                    $product_price = $contract_price;
                }

                $attributes['products'][] = array(
                    'id' => $product_id,
                    'sku' => $product_sku,
                    'name' => $product_name,
                    'price' => $product_price,
                    'subtotal' => $product_subtotal,
                    'qty' => $product_qty
                );
            }
        }

        return $this->get_template_html( 'jde-checkout', $attributes );
    }

    /**
     * @param $attributes
     * @param null $content
     */
    public function render_jde_shopping_cart($attributes,$content=null){
        // Parse short code attributes
        $default_attributes = array();// array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );


        $attributes['redirect'] = '';
        if ( !is_user_logged_in() || $this->shouldRebind()  ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        } else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }
            // get all shopping products
            require_once ( get_stylesheet_directory()  . '/includes/jde-internal-user.php');
            require_once ( get_stylesheet_directory()  . '/includes/jde-customer-ref.php');
            $jdeInternalUserObj = new JdeInternalUser();
            $jdeCustomerRefObj = new JdeCustomerRef();
            $my_customer_id = $jdeInternalUserObj->getMyCustomerId();
            $attributes['products'] = array();
            $all_total = 0;
            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
                $product_thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                $product_removeurl = esc_url( WC()->cart->get_remove_url( $cart_item_key ) );
                $product_sku = $_product->get_sku();
                $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                $product_qty = $cart_item['quantity'];
                $product_visible = apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key);
                $product_name = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key);

                $product_price =  $_product->get_price(); //wc_get_price_including_tax($_product);// apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                //$product_meta = WC()->cart->get_item_data( $cart_item );

                // in case there are special product price , we should re-set price and subtotal here
                $contract_price = $jdeCustomerRefObj->getCrossPrice($my_customer_id,$product_sku);
                if ( $contract_price > 0 ) {
                    $product_price = $contract_price;
                }
                $product_subtotal = $product_price * $product_qty;//apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );

                // get product extra attributes
                $atts = $_product->get_attributes();
                $meta = array();
                foreach ( $atts as $k => $av ) {
                    $opts = $av->get_options();
                    $meta[$k] = implode(' ' , $opts);
                }

                $attributes['products'][] = array(
                    'id' => $product_id,
                    'sku' => $product_sku,
                    'country' => $_product->get_attribute('country'),
                    'name' => $product_name,
                    'price' => $product_price,
                    'subtotal' => $product_subtotal,
                    'min_unit' => $_product->get_attribute('min_unit'),
                    'qty' => $product_qty,
                    'thumbnail' => $product_thumbnail,
                    'remove_link' => $product_removeurl,
                    'product_link' => $product_permalink,
                    'visible' => $product_visible,
                    'cart_item_key' => $cart_item_key,
                    'meta' => $meta
                );

                $all_total += $product_subtotal;
            }

            $attributes['total'] = $all_total;
            $attributes['show_price'] = $jdeInternalUserObj->canSeePrice();
        }


        return $this->get_template_html( 'jde-shopping-cart', $attributes );
    }

    /**
     * @param $attributes
     * @param null $content
     * @return string|void
     */
    public function execute_order_email($attributes, $content = null){

        $order_id = isset($_POST['oid']) ? $_POST["oid"] : '';
        if ( empty($order_id) ) {
            $order_id = isset($_GET['oid']) ? $_GET["oid"] : '';
        }
        write_log('send execute order email now for order id :' . $order_id);

        require_once ( get_stylesheet_directory()  . '/includes/jde-order.php' );
        $jdeOrderObj = new JdeOrder();
        // notify customer this order
        $jdeOrderObj->notifyCustomerOrderChangedByEmail($order_id);

        echo json_encode( array('rt' => 'done'));

    }

     /**
     * @param $attributes
     * @param null $content
     * @return string|void
     */
    public function render_purchaser_home_cross($attributes, $content = null){

        write_log( 'user :' . get_current_user_id() . ' enter purchaser home cross');
        // Parse shortcode attributes
        $default_attributes = array();// array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );


        $attributes['redirect'] = '';
        $attributes['products'] = array();
        require_once ( get_stylesheet_directory() . '/includes/jde-utils.php');
        if ( !is_user_logged_in()  || $this->shouldRebind() ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        } else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }
            $products = array();
            write_log( 'user :' . get_current_user_id() . ' get cross products');
            // get all customer cross reference products
            require_once ( get_stylesheet_directory()  . '/includes/jde-internal-user.php' );
            require_once ( get_stylesheet_directory()  . '/includes/jde-customer-ref.php' );
            $internalUserObj = new JdeInternalUser();
            $iuser = $internalUserObj->get_by(['wp_user_id' => get_current_user_id()]);
            $dest_ids = array();
            if ( !empty($iuser) ) {
                $crossUserObj = new JdeCustomerRef();
                $crossUserProducts = $crossUserObj->get_by(['jde_customer_id' => $iuser[0]->jde_customer_id]);
                if ( !empty($crossUserProducts) ) {
                    foreach ( $crossUserProducts as $productRef ) {
                        $pid = wc_get_product_id_by_sku($productRef->jde_product_id);
                        if ( !empty($pid) ) $dest_ids[] = $pid;
                    }
                }
            }

            if ( !empty($dest_ids) ){

                // try to get all related en or zh-hans product id as well
                // otherwise customer will can see the contract products maybe when switch between different languages
                $others_lang_ids = JdeUtils::get_all_translated_ids($dest_ids);
                $dest_ids = array_merge($dest_ids,$others_lang_ids);
                $dest_in_ids = implode(',',$dest_ids);

                $attributes['products'] = JdeUtils::get_query_products('','ASC',$dest_in_ids,0);;
            }
            
            write_log( 'user :' . get_current_user_id() . ' get cross products done');

            $attributes['show_price'] = $internalUserObj->canSeePrice();
        }

        return $this->get_template_html( 'purchaser-home-cross', $attributes );
    }

    /**
     * @param $attributes
     * @param null $content
     * @return string|void
     */
    public function render_purchaser_home($attributes, $content = null){
        // Parse shortcode attributes
        $default_attributes = array();// array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );


        $attributes['redirect'] = '';
        $attributes['products'] = array();
        if ( !is_user_logged_in()  || $this->shouldRebind() ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        } else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }
            // get all products
            $args = array(
                'status'   => array( 'private', 'publish' ),
                'orderby'  => array('post_title'   => 'ASC'),
                'return'   => 'objects',
                'limit' => self::LOADING_MORE_PRODUCTS_MAX,
                'paged' => 1,
                'paginate' => false,
            );
            $products = wc_get_products( $args );


            // check to see if the customer can see the price
            require_once ( get_stylesheet_directory()  . '/includes/jde-internal-user.php');
            $jdeInternalUserObj = new JdeInternalUser();
            $attributes['show_price'] = $jdeInternalUserObj->canSeePrice();

            $attributes['products'] = $products;

        }

        include_once(get_stylesheet_directory().'/includes/jde-utils.php');
        return $this->get_template_html( 'purchaser-home', $attributes );
    }


    /**
     * A shortcode for rendering the login form.
     *
     * @param  array   $attributes  Shortcode attributes.
     * @param  string  $content     The text content for shortcode. Not used.
     *
     * @return string  The shortcode output
     */
    public function render_login_form( $attributes, $content = null ) {

        // Parse shortcode attributes
        $default_attributes = array();// array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );

        // anyway we should check login or not?
        $b_check_login = true;
        if ( isset($_GET['out']) ) $b_check_login = false;

        // Pass the redirect parameter to the WordPress login functionality: by default,
        // don't specify a redirect, but if a valid redirect URL has been passed as
        // request parameter, use it.
        $attributes['redirect'] = '';
        if ( is_user_logged_in() && $b_check_login ) {
            $wxid = '';
            if ( isset($_GET['wxid']) ) {
                $wxid = $_GET['wxid'];
            }
            //return __( 'You are already signed in.', 'jde-login' );
            // we need to check if user bind with wp user
            require_once ( get_stylesheet_directory()  . '/includes/jde-user.php' );
            $userObj = new JdeUser();
            $user = $userObj->get_by(['wx_openid' => $wxid] );
            if ( !empty($user) ) {
                $attributes['redirect'] = esc_url(home_url() . '/purchaser-home-cross');
            }
        } else {
            if (isset($_REQUEST['redirect_to'])) {
                $attributes['redirect'] = wp_validate_redirect($_REQUEST['redirect_to'], $attributes['redirect']);
            }
        }

        // Error messages
        $errors = array();
        if ( isset( $_REQUEST['login'] ) ) {
            $error_codes = explode( ',', $_REQUEST['login'] );

            foreach ( $error_codes as $code ) {
                $errors []= $this->get_error_message( $code );
            }
        }
        $attributes['errors'] = $errors;

        if ( isset($_GET['wxid']) ) {
            write_log('yx-test-gotwxid-inform : '.$_GET['wxid']);
            $attributes['wxid'] = $_GET['wxid'];
        } else {
            if ( !isset($_GET['admin'] )) {
                $attributes['redirect'] = get_stylesheet_directory_uri() . '/wx.php?d=shop';
            }
        }
        if ( !empty($errors) ) {
            write_log('yx-test-inform errors : '. print_r($errors,true));
        }
        // Check if user just logged out
        $attributes['logged_out'] = isset( $_REQUEST['logged_out'] ) && $_REQUEST['logged_out'] == true;

        // avoid dead loop
        if ( !empty($attributes['redirect']) ) {
            if ( strstr($attributes['redirect'],'member-login') !== FALSE ) {
                $attributes['redirect'] = '';
            }
        }

        // Render the login form using an external template
        return $this->get_template_html( 'login-form', $attributes );
    }

    /**
     * admin edit user form
     * @param $attributes
     * @param null $content
     * @return string
     */
    public function render_admin_edit_user( $attributes, $content = null ) {
        // Parse short code attributes
        $default_attributes = array();
        $attributes = shortcode_atts( $default_attributes, $attributes );

        $attributes['redirect'] = '';
        $attributes['users'] = array();
        $attributes['roles'] = array();
        $attributes['customers'] = array();

        if ( !is_user_logged_in()  || $this->shouldRebind() ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        } else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }



            // get all user roles
            require_once ( get_stylesheet_directory()  . '/includes/jde-internal-user.php' );
            $attributes['roles'] = JdeInternalUser::$JDE_ROLES;

            // check to see if user has edit users right
            $interUserObj = new JdeInternalUser();
            $attributes['can_edit_users'] = $interUserObj->canEditUsers();

            // get all customer list
            require_once ( get_stylesheet_directory()  . '/includes/jde-customer.php' );
            $customer = new JdeCustomer();
            $attributes['customers'] = $customer->getCustomers();

            // get all jde internal user list
            require_once ( get_stylesheet_directory()  . '/includes/jde-internal-user.php' );
            $iuser = new JdeInternalUser();
            $attributes['users'] = $iuser->getUsers();
        }

        return $this->get_template_html( 'admin-edit-user', $attributes );

    }

    /**
     * render admin setting form
     * @param $attributes
     * @param null $content
     * @return string
     */
    public function render_admin_setting( $attributes, $content = null ) {
        // Parse short code attributes
        $default_attributes = array();
        $attributes = shortcode_atts( $default_attributes, $attributes );

        $error = '';
        $attributes['redirect'] = '';
        if ( !is_user_logged_in()  || $this->shouldRebind() ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        }  else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }
        }

        if ( !empty($error) ) {
            $attributes['error'] = $error;
        }

        return $this->get_template_html( 'admin-setting', $attributes );

    }

    /**
     * @param $attributes
     * @param null $content
     * @return string
     */
    public function render_admin_order_history( $attributes, $content = null ) {
        // Parse short code attributes
        $default_attributes = array();
        $attributes = shortcode_atts( $default_attributes, $attributes );

        $error = '';
        $attributes['redirect'] = '';
        if ( !is_user_logged_in()  || $this->shouldRebind() ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        }  else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }

            // get all history orders this people can view
            // get all pending orders
            require_once(get_stylesheet_directory()  . '/includes/jde-order.php');
            $jdeOrderObj = new JdeOrder();
            $attributes['orders'] = $jdeOrderObj->getHistoryOrders();
        }

        if ( !empty($error) ) {
            $attributes['error'] = $error;
        }

        return $this->get_template_html( 'admin-order-history', $attributes );

    }

    /**
     * @param $attributes
     * @param null $content
     * @return string
     */
    public function render_admin_pending_order( $attributes, $content = null ) {
        // Parse short code attributes
        $default_attributes = array();
        $attributes = shortcode_atts( $default_attributes, $attributes );

        $attributes['redirect'] = '';

        if ( !is_user_logged_in()  || $this->shouldRebind() ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        } else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
            // }

            // get all pending orders
            require_once(get_stylesheet_directory()  . '/includes/jde-order.php');
            $jdeOrderObj = new JdeOrder();
            $attributes['orders'] = $jdeOrderObj->getAllPendingOrders();
        }

        return $this->get_template_html( 'admin-pending-order', $attributes );

    }

    /**
     * @param $attributes
     * @param null $content
     * @return string|void
     */
    public function render_admin_access( $attributes, $content = null ) {
        // Parse short code attributes
        $default_attributes = array();
        $attributes = shortcode_atts( $default_attributes, $attributes );

        $attributes['error'] = '';
        $attributes['redirect'] = '';
        if ( !is_user_logged_in()  || $this->shouldRebind() ) {
            $attributes['redirect'] = $this->getMemberloginUrl();
        }  else {
            // we need check user right to see if she or he can access administrator page
            // only user with admin right can access to this page
            // TODO soon
            //if ( !admin ) {
            //    $error = __( 'You are not right to access admin page.', 'jde-login' );
           // }

            // check to see if the user has access right
            require_once(get_stylesheet_directory()  . '/includes/jde-internal-user.php');
            $jdeInternalUserObj = new JdeInternalUser();
            if ( !$jdeInternalUserObj->canAccessAdmin() ) {
                $attributes['error'] = __('Sorry, You do not have access right','jde-admin');
            }
        }


        // Render the login form using an external template
        return $this->get_template_html( 'admin-access', $attributes );

    }

    /**
     * Renders the contents of the given template to a string and returns it.
     *
     * @param string $template_name The name of the template to render (without .php)
     * @param array  $attributes    The PHP variables for the template
     *
     * @return string               The contents of the template.
     */
    private function get_template_html( $template_name, $attributes = null ) {
        if ( !$attributes ) {
            $attributes = array();
        }

        ob_start();
        do_action( 'jde_login_before_' . $template_name );
        require( 'templates/' . $template_name . '.php');
        do_action( 'jde_login_after_' . $template_name );
        $html = ob_get_contents();
        ob_end_clean();

        write_log( 'user :' . get_current_user_id() . ' render :' . $template_name . ' done');

        return $html;
    }

    /**
     * Plugin activation hook.
     *
     * Creates all WordPress pages needed by the plugin.
     */
    public static function plugin_activated() {
        // add our register and account information page
        // Information needed for creating the plugin's pages
        $page_definitions = array(
            'member-login' => array(
                'title' => __( 'Sign In', 'jde-login-no-translation' ),
                'content' => '[custom-login-form]'
            ),
            'member-account' => array(
                'title' => __( 'Your Account', 'jde-login-no-translation' ),
                'content' => '[account-info]'
            ),

            'order-notify-email' => array(
                'title' => __( 'Order Notify Email', 'jde-login-no-translation' ),
                'content' => '[order-notify-email]'
            ),
            'jde-execute-order-email' => array(
                'title' => __( 'jde-execute-order-email', 'jde-login-no-translation' ),
                'content' => '[jde-execute-order-email]'
            ),
            // [admin-access-form]	Admin Access
            'admin-access' => array(
                'title' => __( 'Admin Access', 'jde-login-no-translation' ),
                'content' => '[admin-access-form]'
            ),

            //  [admin-edit-user-form]	Admin Edit User
            'admin-edit-user' => array(
                'title' => __( 'Admin Edit User', 'jde-login-no-translation' ),
                'content' => '[admin-edit-user-form]'
            ),

            // [admin-setting-form]	Admin Setting
            'admin-setting' => array(
                'title' => __( 'Admin Setting', 'jde-login-no-translation' ),
                'content' => '[admin-setting-form]'
            ),

            // [admin-pending-order-form]	Admin Pending Order
            'admin-pending-order' => array(
                'title' => __( 'Admin Pending Order', 'jde-login-no-translation' ),
                'content' => '[admin-pending-order-form]'
            ),

            //  [admin-order-history-form]	Admin Order History
            'admin-order-history' => array(
                'title' => __( 'Admin Order History', 'jde-login-no-translation' ),
                'content' => '[admin-order-history-form]'
            ),

            // [jde-purchaser-home-form]	Purchaser Home
            'purchaser-home' => array(
                'title' => __( 'Purchaser Home', 'jde-login-no-translation' ),
                'content' => '[jde-purchaser-home-form]'
            ),

            // [jde-shopping-cart]	Jde Shopping Cart
            'jde-shopping-cart' => array(
                'title' => __( 'Jde Shopping Cart', 'jde-login-no-translation' ),
                'content' => '[jde-shopping-cart]'
            ),

            // [jde-checkout-form]	JDE Checkout
            'jde-checkout' => array(
                'title' => __( 'Jde Checkout', 'jde-login-no-translation' ),
                'content' => '[jde-checkout-form]'
            ),

            // [jde-current-order]	Jde current order
            'jde-current-order' => array(
                'title' => __( 'Jde current order', 'jde-login-no-translation' ),
                'content' => '[jde-current-order]'
            ),

            // [jde-order-history]	JDE order history
            'jde-order-history' => array(
                'title' => __( 'Jde order history', 'jde-login-no-translation' ),
                'content' => '[jde-order-history]'
            ),

            // [jde-contact-us-form]	JDE contact us
            'jde-contact-us' => array(
                'title' => __( 'Jde Contact Us', 'jde-login-no-translation' ),
                'content' => '[jde-contact-us-form]'
            ),

            // [jde-user-setting-form]	JDE user setting
            'jde-user-setting' => array(
                'title' => __( 'JDE user setting', 'jde-login-no-translation' ),
                'content' => '[jde-user-setting-form]'
            ),

            // [jde-purchaser-home-cross-form]	Purchaser home cross
            'purchaser-home-cross' => array(
                'title' => __( 'Purchaser Home Cross', 'jde-login-no-translation' ),
                'content' => '[jde-purchaser-home-cross-form]'
            ),

        );

        foreach ( $page_definitions as $slug => $page ) {
            // Check that the page doesn't exist already
            $query = new WP_Query( 'pagename=' . $slug );
            if ( ! $query->have_posts() ) {
                // Add the page using the data from the array above
                wp_insert_post(
                    array(
                        'post_content'   => $page['content'],
                        'post_name'      => $slug,
                        'post_title'     => $page['title'],
                        'post_status'    => 'publish',
                        'post_type'      => 'page',
                        'ping_status'    => 'closed',
                        'comment_status' => 'closed',
                    )
                );
            }
        }

        // create our customer tables
        // create Wechat user bind table
        self::createJdeUserTable();

        // create jde customer table
        self::createJdeCustomerTable();

        // create jde customer address table
        self::createJdeCustomerAddrTable();

        // create jde customer cross reference products table
        self::createJdeCustomerCrossRefTable();

        // create JDE order delete lines table
        self::createJdeDeleteOrderLineTable();

        // create JDE internal jde user table
        self::createJdeInternalUserTable();

        // create email notify schedule for orders table
        self::createJdeEmailNotifyTable();

        // set jde hourly event
        if ( !wp_next_scheduled( self::JDE_SYNC_DAILY_CRON_EVENT) ) {
            wp_schedule_event(strtotime('05:30:00'),'daily', self::JDE_SYNC_DAILY_CRON_EVENT);
        }
        if ( !wp_next_scheduled( self::JDE_NOTIFY_DAILY_CRON_EVENT) ) {
            wp_schedule_event(strtotime('09:30:00'),'daily', self::JDE_NOTIFY_DAILY_CRON_EVENT);
        }
        if ( !wp_next_scheduled( self::JDE_NOTIFY_2nd_DAILY_CRON_EVENT) ) {
            wp_schedule_event(strtotime('10:30:00'),'daily', self::JDE_NOTIFY_2nd_DAILY_CRON_EVENT);
        }
        if ( !wp_next_scheduled( self::JDE_NOTIFY_3rd_DAILY_CRON_EVENT) ) {
            $date = new DateTime( '2018-09-06 17:00:00', new DateTimeZone( get_option( 'timezone_string' ) ) );
            $timestamp = $date->getTimestamp();
            wp_schedule_event($timestamp,'daily', self::JDE_NOTIFY_3rd_DAILY_CRON_EVENT);
        }
        if ( !wp_next_scheduled( self::JDE_SCAN_EMAIL_CRON_EVENT) ) {
            wp_schedule_event(time(),self::JDE_CRON_ONE_MINUTE_RECURRENCE, self::JDE_SCAN_EMAIL_CRON_EVENT);
        }

    }

    /**
     * create jde every hour recurrence interval
     * @param $schedules
     * @return mixed
     */
    function jde_add_cron_recurrence_interval( $schedules ) {
        $schedules[self::JDE_CRON_HALF_HOUR_RECURRENCE] = array(
            'interval'  => 1800,
            'display'   => __( 'Every Half Hour', 'jde-login' )
        );
        return $schedules;
    }


    /**
     * create jde every hour recurrence interval
     * @param $schedules
     * @return mixed
     */
    function jde_add_cron_recurrence_interval_onemin( $schedules ) {
        $schedules[self::JDE_CRON_ONE_MINUTE_RECURRENCE] = array(
            'interval'  => 60,
            'display'   => __( 'Every One Mintue', 'jde-login' )
        );
        return $schedules;
    }

    /**
     * do jde plugin deactive job
     */
    public static function plugin_deactivated() {
        // delete sign in page do we need ?
        //  wp_delete_post();

        // remove our cron
        wp_clear_scheduled_hook('jde_daily_event');
    }

    /**
     *
     */
    public static function createJdeEmailNotifyTable(){
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'jde_email_notify';

        $sql = "CREATE TABLE $table_name (
		  id int(10) unsigned NOT NULL AUTO_INCREMENT,
          order_id int(11) DEFAULT '0',
          notify_type int(11) DEFAULT '0',
          flag int(11) DEFAULT '0',
          PRIMARY KEY (`id`),
          KEY `idx_order_id` (`order_id`),
          KEY `idx_flag` (`flag`)
	    ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    /**
     *
     */
    public static function createJdeInternalUserTable(){
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'jde_internal_user';

        $sql = "CREATE TABLE $table_name (
		  id int(10) unsigned NOT NULL AUTO_INCREMENT,
          wp_user_id int(11) DEFAULT '0',
          jde_customer_id int(11) DEFAULT '0',
          username varchar(45) DEFAULT NULL,
          password varchar(45) DEFAULT NULL,
          user_group varchar(45) DEFAULT NULL,
          meta varchar(450) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `wp_user_id` (`wp_user_id`),
          KEY `idx_username` (`username`),
          KEY `idx_group` (`user_group`),
          KEY `idx_jdecustomer_id` (`jde_customer_id` ASC)
	    ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    /**
     * create JDE order delete lines table
     */
    public static function createJdeDeleteOrderLineTable(){

        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'jde_order_line_delete';

        $sql = "CREATE TABLE $table_name (
		id int(11) NOT NULL AUTO_INCREMENT,
		order_id INT NULL,
		order_line_id INT NULL,
        product_id INT NULL,
        product_name VARCHAR(145) NULL,
        qty INT NULL,
        price DECIMAL(10,3) NULL,
        delete_time DATETIME NULL,
		PRIMARY KEY id (id),
		INDEX `idx_orderid` (`order_id` ASC),
		INDEX `idx_lineorderid` (`order_line_id` ASC)
	    ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

    }

    /**
     * create JDE user table
     */
    public static function createJdeUserTable()
    {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'jde_user';

        $sql = "CREATE TABLE $table_name (
		id int(11) NOT NULL AUTO_INCREMENT,
		wp_user_id int(11) DEFAULT 0 NOT NULL,
		wx_openid varchar(45) NOT NULL,
		jde_customer_id int(11) DEFAULT 0 NOT NULL,
		added_time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		PRIMARY KEY id (id),
		KEY `idx_wxopeind` (`wx_openid`),
		KEY `idx_wpuserid` (`wp_user_id`),
		KEY `idx_jdecustomerid` (`jde_customer_id`)
	    ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        // $version = get_option( 'jde_login_plugin_version', '1.0' );
        // if ( version_compare( $version, '2.0' ) < 0 ) {
        //     update_option( 'jde_login_plugin_version', '2.0' );
        // }

    }

    /**
     * create customer table for jde
     */
    public static function createJdeCustomerTable()
    {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'jde_customer';

        $sql = "CREATE TABLE $table_name (
          id INT NOT NULL AUTO_INCREMENT,
		  jde_customer_id INT NOT NULL,
          cname VARCHAR(45) NULL,
          ename VARCHAR(45) NULL,
          email VARCHAR(45) NULL,
          notify_email VARCHAR(45) NULL,
          telephone VARCHAR(15) NULL,
          status TINYINT(2) NULL,
          pay_methods VARCHAR(80) NULL,
          office VARCHAR(45) NULL,
          note VARCHAR(120) NULL,
          warehouse VARCHAR(20) NULL,
          region VARCHAR(45) NULL,
          channel VARCHAR(45) NULL,
          sub_channel VARCHAR(45) NULL,
          added_time DATETIME NULL,
          PRIMARY KEY (`id`),
          INDEX `idx_customerid` (`jde_customer_id` ASC),
          INDEX `idx_cname` (`cname` ASC),
          INDEX `idx_telephone` (`telephone` ASC),
          INDEX `idx_status` (`status` ASC),
          INDEX `idx_channel` (`channel` ASC),
          INDEX `idx_sub_channel` (`sub_channel` ASC),
          INDEX `idx_added_time` (`added_time` ASC)
	    ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    /**
     * create jde customer address table
     */
    public static function createJdeCustomerAddrTable()
    {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'jde_customer_addr';

        $sql = "CREATE TABLE $table_name (
		  id INT NOT NULL AUTO_INCREMENT,
          jde_customer_id INT NULL,
          jde_address_id INT NULL,
          name VARCHAR(45) NULL,
          state VARCHAR(15) NULL,
          city VARCHAR(45) NULL,
          suburb VARCHAR(45) NULL,
          address VARCHAR(245) NULL,
          postcode VARCHAR(10) NULL,
          telephone VARCHAR(15) NULL,
          country VARCHAR(15) NULL,
          PRIMARY KEY (`id`),
          INDEX `idx_customerid` (`jde_customer_id` ASC),
          INDEX `idx_address_id` (`jde_address_id` ASC),
          INDEX `idx_name` (`name` ASC),
          INDEX `idx_telephone` (`telephone` ASC)
	    ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    /**
     * create jde customer cross reference product table
     */
    public static function createJdeCustomerCrossRefTable()
    {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'jde_customer_cross_ref';

        $sql = "CREATE TABLE $table_name (
		  id INT NOT NULL AUTO_INCREMENT,
          jde_customer_id INT NULL,
          jde_product_id VARCHAR(20)  NULL,
          contract_price DECIMAL(10,3) NULL DEFAULT 0,
          PRIMARY KEY (`id`),
          INDEX `idx_jdecustomerid` (`jde_customer_id` ASC),
          INDEX `idx_jdeproductid` (`jde_product_id` ASC)
	    ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

// Initialize the plugin
$jde_login_pages_plugin = new JDE_Login_Plugin();

// Create the custom pages at plugin activation
register_activation_hook( __FILE__, array( 'JDE_Login_Plugin', 'plugin_activated' ) );

// once deactivated do some clean job
register_deactivation_hook( __FILE__,array( 'JDE_Login_Plugin', 'plugin_deactivated' ) );
